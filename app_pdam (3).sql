-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 06 Agu 2020 pada 11.48
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `app_pdam`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` char(32) NOT NULL,
  `id_tipe_admin` varchar(3) NOT NULL,
  `jn_admin` varchar(2) NOT NULL,
  `kd_admin` varchar(32) NOT NULL,
  `id_bumdes` varchar(32) NOT NULL,
  `email` text NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(256) NOT NULL,
  `status_active` enum('0','1','2') NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `nip_admin` varchar(64) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `id_tipe_admin`, `jn_admin`, `kd_admin`, `id_bumdes`, `email`, `username`, `password`, `status_active`, `nama_admin`, `nip_admin`, `is_delete`) VALUES
('03XX202007050000008', '0', '0', '02XX.00000', '05XX2020070500007', 'suryahanggara@gmail.com', 'surya', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'surya', 'a', '0'),
('03A202007050000009', '0', '1', '02A.00001', '05A2020070500006', 'kabgresik@gmail.com', 'kabgresik', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'kabgresik', 'a', '0'),
('03B202007070000010', '0', '1', '02B.00001', '05B2020070700008', 'kab.banyuwangi@gmail.com', 'kabbanyuwangi', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'kab.banyuwangi', 'a', '0');

--
-- Trigger `admin`
--
DROP TRIGGER IF EXISTS `gan_insert_adm`;
DELIMITER //
CREATE TRIGGER `gan_insert_adm` BEFORE INSERT ON `admin`
 FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE tipe_data VARCHAR(32);
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index
    INTO rowcount
    FROM index_number where id = 'admin';
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'admin';
    
    SELECT kd_bumdes INTO tipe_data FROM m_bumdes where id_bumdes = NEW.id_bumdes;
    
    SET fix_key_user = concat("03", tipe_data, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_admin = fix_key_user;
    SET NEW.kd_admin = concat("02", tipe_data, ".", NEW.kd_admin);
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `index_number`
--

CREATE TABLE IF NOT EXISTS `index_number` (
  `id` varchar(32) NOT NULL,
  `id_index` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `index_number`
--

INSERT INTO `index_number` (`id`, `id_index`) VALUES
('acc_prj', '15'),
('admin', '10'),
('article_main', '27'),
('m_bumdes', '9'),
('m_pengeluaran', '11'),
('m_petugas', '14'),
('m_prj', '48'),
('m_user', '11'),
('tr_tagihan', '11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_bumdes`
--

CREATE TABLE IF NOT EXISTS `m_bumdes` (
  `id_bumdes` varchar(32) NOT NULL,
  `nm_bumdes` text NOT NULL,
  `kd_bumdes` varchar(32) NOT NULL,
  `alamat_bumdes` text NOT NULL,
  `tarif_bumdes` varchar(32) NOT NULL,
  `is_del_bumdes` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_bumdes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_bumdes`
--

INSERT INTO `m_bumdes` (`id_bumdes`, `nm_bumdes`, `kd_bumdes`, `alamat_bumdes`, `tarif_bumdes`, `is_del_bumdes`) VALUES
('05A2020070500006', 'BUMDES Kab. Gresik', 'A', 'Gresik', '10000', '0'),
('05B2020070700008', 'BUMDES Kab. Banyuwangi', 'B', 'Banyuwangi', '12000', '0'),
('05N2020070800009', 'BUMDES Kab.Malang', 'N', 'Malang', '11000', '0'),
('05XX2020070500007', 'Admin', 'XX', 'Surabaya', '0', '0');

--
-- Trigger `m_bumdes`
--
DROP TRIGGER IF EXISTS `gan_insert_m_bumdes`;
DELIMITER //
CREATE TRIGGER `gan_insert_m_bumdes` BEFORE INSERT ON `m_bumdes`
 FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE tipe_data VARCHAR(32);
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index
    INTO rowcount
    FROM index_number where id = 'm_bumdes';
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'm_bumdes';
    
    set tipe_data = NEW.kd_bumdes;
    SET fix_key_user = concat("05", tipe_data, left(NOW()+0, 8), LPAD(rowcount_next, 5, '0'));
    
    SET NEW.id_bumdes = fix_key_user;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_petugas`
--

CREATE TABLE IF NOT EXISTS `m_petugas` (
  `id_petugas` varchar(32) NOT NULL,
  `id_bumdes` varchar(32) NOT NULL,
  `jn_admin` varchar(2) NOT NULL,
  `username_petugas` varchar(32) NOT NULL,
  `pass_petugas` varchar(64) NOT NULL,
  `kd_petugas` varchar(32) NOT NULL,
  `nm_petugas` text NOT NULL,
  `almt_petugas` text NOT NULL,
  `nik_petugas` varchar(32) NOT NULL,
  `created_by_petugas` varchar(64) NOT NULL,
  `date_created_petugas` datetime NOT NULL,
  `is_del_petugas` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_petugas`
--

INSERT INTO `m_petugas` (`id_petugas`, `id_bumdes`, `jn_admin`, `username_petugas`, `pass_petugas`, `kd_petugas`, `nm_petugas`, `almt_petugas`, `nik_petugas`, `created_by_petugas`, `date_created_petugas`, `is_del_petugas`) VALUES
('03A202007050000013', '05A2020070500006', '2', 'tirto', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '03A.0000001', 'tirto', 'malang', '123123123', '03A202007050000009', '2020-07-05 06:44:54', '0'),
('03B202007080000014', '05B2020070700008', '2', 'samox', 'ea1b9d779a37fa378d87c40dd6a56fcd491a7c9bef3a1f6e40228031bf00ac68', '03B.0000001', 'samox', 'kab banyuwangix', '1231234', '03B202007070000010', '2020-07-08 12:45:02', '0');

--
-- Trigger `m_petugas`
--
DROP TRIGGER IF EXISTS `gan_insert_m_petugas`;
DELIMITER //
CREATE TRIGGER `gan_insert_m_petugas` BEFORE INSERT ON `m_petugas`
 FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE tipe_data VARCHAR(32);
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index INTO rowcount FROM index_number where id = 'm_petugas'; SEt rowcount_next = rowcount + 1; UPDATE index_number SET id_index=rowcount_next WHERE id = 'm_petugas';
    
    SELECT kd_bumdes INTO tipe_data FROM m_bumdes where id_bumdes = NEW.id_bumdes;

SET fix_key_user = concat("03", tipe_data, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));

SET NEW.id_petugas = fix_key_user; END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_user`
--

CREATE TABLE IF NOT EXISTS `m_user` (
  `id_user` varchar(32) NOT NULL,
  `id_bumdes` varchar(32) NOT NULL,
  `jn_admin` varchar(2) NOT NULL,
  `username_user` varchar(32) NOT NULL,
  `pass_user` varchar(64) NOT NULL,
  `kd_user` varchar(32) NOT NULL,
  `nm_user` text NOT NULL,
  `almt_user` text NOT NULL,
  `nik_user` varchar(32) NOT NULL,
  `disc_user` varchar(6) NOT NULL,
  `created_by_user` varchar(64) NOT NULL,
  `date_created_user` datetime NOT NULL,
  `is_del_user` enum('0','1') NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_user`
--

INSERT INTO `m_user` (`id_user`, `id_bumdes`, `jn_admin`, `username_user`, `pass_user`, `kd_user`, `nm_user`, `almt_user`, `nik_user`, `disc_user`, `created_by_user`, `date_created_user`, `is_del_user`) VALUES
('02A202007060000007', '05A2020070500006', '3', 'toni', '04f8996da763b7a969b1028ee3007569eaf3a635486ddab211d512c85b9df8fb', '01A.0000001', 'toni', 'malang', '123213123', '10.5', '03A202007050000009', '2020-07-06 12:48:35', '0'),
('02A202007310000011', '05A2020070500006', '3', 'doni', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '01A.0000003', 'doni', 'malang', '123123', '5.2', '03A202007050000009', '2020-07-31 07:38:57', '0'),
('02B202007080000008', '05B2020070700008', '3', 'tono', '04f8996da763b7a969b1028ee3007569eaf3a635486ddab211d512c85b9df8fb', '01B.0000001', 'tono', 'banyuwangi', '123123', '1.5', '03B202007070000010', '2020-07-08 12:45:37', '0'),
('02N202007310000009', '05N2020070800009', '3', 'test', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '01N.0000012', 'test', 'malang', '123', '1.5', '03XX202007050000008', '2020-07-31 05:44:38', '0'),
('02N202007310000010', '05N2020070800009', '3', 'test1', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '01N.0000003', 'test', 'malang', '12312', '10', '03XX202007050000008', '2020-07-31 05:47:24', '0');

--
-- Trigger `m_user`
--
DROP TRIGGER IF EXISTS `gan_insert_m_user`;
DELIMITER //
CREATE TRIGGER `gan_insert_m_user` BEFORE INSERT ON `m_user`
 FOR EACH ROW BEGIN DECLARE rowcount INT; 
DECLARE tipe_data VARCHAR(32);
DECLARE rowcount_next INT; DECLARE fix_key_user VARCHAR(32); SELECT id_index INTO rowcount FROM index_number where id = 'm_user'; SEt rowcount_next = rowcount + 1; UPDATE index_number SET id_index=rowcount_next WHERE id = 'm_user';

SELECT kd_bumdes INTO tipe_data FROM m_bumdes where id_bumdes = NEW.id_bumdes;
SET fix_key_user = concat("02", tipe_data, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0')); 

SET NEW.id_user = fix_key_user; END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_pengeluaran`
--

CREATE TABLE IF NOT EXISTS `tr_pengeluaran` (
  `id_tr_pengeluaran` varchar(32) NOT NULL,
  `id_bumdes` varchar(32) NOT NULL,
  `tgl_tr_pengeluaran` date NOT NULL,
  `nm_tr_pengeluaran` text NOT NULL,
  `nominal_tr_pengeluaran` varchar(32) NOT NULL,
  `ket_tr_pengeluaran` text NOT NULL,
  `tgl_inp_tr_pengeluaran` datetime NOT NULL,
  `create_by_tr_pengeluaran` varchar(32) NOT NULL,
  `tgl_up_tr_pengeluaran` datetime NOT NULL,
  `up_by_tr_pengeluaran` varchar(32) NOT NULL,
  PRIMARY KEY (`id_tr_pengeluaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tr_pengeluaran`
--

INSERT INTO `tr_pengeluaran` (`id_tr_pengeluaran`, `id_bumdes`, `tgl_tr_pengeluaran`, `nm_tr_pengeluaran`, `nominal_tr_pengeluaran`, `ket_tr_pengeluaran`, `tgl_inp_tr_pengeluaran`, `create_by_tr_pengeluaran`, `tgl_up_tr_pengeluaran`, `up_by_tr_pengeluaran`) VALUES
('07A2020070700000006', '05A2020070500006', '2020-07-02', 'test kab gresik 2', '20000000', 'test kab gresik 2', '2020-07-07 06:11:00', '03A202007050000009', '2020-07-07 06:11:00', '03A202007050000009'),
('07A2020070700000007', '05A2020070500006', '2020-07-02', 'test kab gresik 2', '15000000', 'test kab gresik 2', '2020-07-07 06:11:28', '03A202007050000009', '2020-07-07 06:11:28', '03A202007050000009'),
('07A2020070700000008', '05A2020070500006', '2020-07-12', 'test kab gresik 3', '1000000', 'test kab gresik 3', '2020-07-07 06:12:02', '03A202007050000009', '2020-07-07 06:12:02', '03A202007050000009'),
('07B2020070700000009', '05B2020070700008', '2020-07-01', 'test banyuwangi 1', '15000000', 'test banyuwangi 1', '2020-07-07 06:14:59', '03B202007070000010', '2020-07-07 06:14:59', '03B202007070000010'),
('07B2020070700000010', '05B2020070700008', '2020-07-02', 'test banyuwangi 2', '4000000', 'test banyuwangi 2', '2020-07-07 06:15:22', '03B202007070000010', '2020-07-07 06:15:22', '03B202007070000010'),
('07B2020070700000011', '05B2020070700008', '2020-07-20', 'test banyuwangi 3', '3000000', 'test banyuwangi 3', '2020-07-07 06:15:42', '03B202007070000010', '2020-07-07 06:15:42', '03B202007070000010');

--
-- Trigger `tr_pengeluaran`
--
DROP TRIGGER IF EXISTS `gan_insert_m_pengeluaran`;
DELIMITER //
CREATE TRIGGER `gan_insert_m_pengeluaran` BEFORE INSERT ON `tr_pengeluaran`
 FOR EACH ROW BEGIN
DECLARE rowcount INT; 
DECLARE tipe_data VARCHAR(32);
DECLARE rowcount_next INT;
DECLARE fix_key_user VARCHAR(32); 

SELECT id_index INTO rowcount FROM index_number where id = 'm_pengeluaran'; SEt rowcount_next = rowcount + 1; 

UPDATE index_number SET id_index=rowcount_next WHERE id = 'm_pengeluaran';

SELECT kd_bumdes INTO tipe_data FROM m_bumdes where id_bumdes = NEW.id_bumdes;

SET fix_key_user = concat("07", tipe_data, left(NOW()+0, 8),"",LPAD(rowcount_next, 8, '0')); 

SET NEW.id_tr_pengeluaran = fix_key_user;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_tagihan`
--

CREATE TABLE IF NOT EXISTS `tr_tagihan` (
  `id_tr_tagihan` varchar(32) NOT NULL,
  `id_bumdes` varchar(32) NOT NULL,
  `id_petugas` varchar(32) NOT NULL,
  `id_user` varchar(32) NOT NULL,
  `tgl_tr_tagihan` date NOT NULL,
  `periode_tr_tagihan` varchar(8) NOT NULL,
  `permeter_tr_tagihan` varchar(32) NOT NULL,
  `meter_tr_tagihan` varchar(32) NOT NULL,
  `disc_tr_tagihan` varchar(10) NOT NULL,
  `nominal_tr_tagihan` varchar(32) NOT NULL,
  `tgl_inp_tr_tagihan` datetime NOT NULL,
  `create_by_tr_tagihan` varchar(32) NOT NULL,
  `sts_pemb` enum('0','1') NOT NULL,
  `vert_pemb_by` varchar(32) NOT NULL,
  `tgl_vert_pemb_by` datetime NOT NULL,
  `tgl_up_tr_tagihan` datetime NOT NULL,
  `up_by_tr_tagihan` varchar(32) NOT NULL,
  PRIMARY KEY (`id_tr_tagihan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tr_tagihan`
--

INSERT INTO `tr_tagihan` (`id_tr_tagihan`, `id_bumdes`, `id_petugas`, `id_user`, `tgl_tr_tagihan`, `periode_tr_tagihan`, `permeter_tr_tagihan`, `meter_tr_tagihan`, `disc_tr_tagihan`, `nominal_tr_tagihan`, `tgl_inp_tr_tagihan`, `create_by_tr_tagihan`, `sts_pemb`, `vert_pemb_by`, `tgl_vert_pemb_by`, `tgl_up_tr_tagihan`, `up_by_tr_tagihan`) VALUES
('10B2020073100000009', '05B2020070700008', '03B202007080000014', '02B202007080000008', '2020-07-31', '2020-11', '12000', '150', '1.5', '1773000', '2020-07-31 08:31:15', '03B202007080000014', '0', '03B202007070000010', '2020-08-05 06:12:53', '2020-07-31 08:31:15', '03B202007080000014'),
('10B2020080500000010', '05B2020070700008', '03B202007080000014', '02B202007080000008', '2020-08-05', '2020-7', '12000', '230', '1.5', '2718600', '2020-08-05 17:41:15', '03B202007080000014', '1', '03B202007070000010', '2020-08-05 06:49:53', '0000-00-00 00:00:00', ''),
('10B2020080500000011', '05B2020070700008', '03B202007080000014', '02B202007080000008', '2020-08-05', '2020-5', '12000', '340', '1.5', '4018800', '2020-08-05 17:47:14', '03B202007080000014', '1', '03B202007070000010', '2020-08-05 05:59:00', '0000-00-00 00:00:00', '');

--
-- Trigger `tr_tagihan`
--
DROP TRIGGER IF EXISTS `insert_tr_tagihan`;
DELIMITER //
CREATE TRIGGER `insert_tr_tagihan` BEFORE INSERT ON `tr_tagihan`
 FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE tipe_data VARCHAR(32);
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index
    INTO rowcount
    FROM index_number where id = 'tr_tagihan';
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'tr_tagihan';
    
    SELECT kd_bumdes INTO tipe_data FROM m_bumdes where id_bumdes = NEW.id_bumdes;
    
    SET fix_key_user = concat("10", tipe_data, left(NOW()+0, 8), LPAD(rowcount_next, 8, '0'));
    
    SET NEW.id_tr_tagihan = fix_key_user;
END
//
DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
