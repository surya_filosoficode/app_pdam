<?php

    $id_tr_tagihan = "";
    $id_bumdes = "";
    $id_petugas = "";
    $id_user = "";
    $tgl_tr_tagihan = "";
    $periode_tr_tagihan = "";
    $permeter_tr_tagihan = "";
    $meter_tr_tagihan = "";
    $nominal_tr_tagihan = "";
    $tgl_inp_tr_tagihan = "";
    $sts_pemb = "";
    $vert_pemb_by = "";
    $tgl_vert_pemb_by = "";
    $tgl_up_tr_tagihan = "";

    $nm_bumdes = "";
    $kd_bumdes = "";
    $alamat_bumdes = "";
    $tarif_bumdes = "";
    $username_petugas = "";
    $kd_petugas = "";
    $nm_petugas = "";
    $almt_petugas = "";
    $nik_petugas = "";
    $kd_user = "";
    $nm_user = "";
    $almt_user = "";
    $disc_user = "";

    $nominal_disc = 0;

	if(isset($list_data)){
		if($list_data){
			// print_r($list_data);
			$id_tr_tagihan = $list_data["id_tr_tagihan"];
		    $id_bumdes = $list_data["id_bumdes"];
		    $id_petugas = $list_data["id_petugas"];
		    $id_user = $list_data["id_user"];
		    $tgl_tr_tagihan = $list_data["tgl_tr_tagihan"];
		    $periode_tr_tagihan = $list_data["periode_tr_tagihan"];
		    $permeter_tr_tagihan = $list_data["permeter_tr_tagihan"];
		    $meter_tr_tagihan = $list_data["meter_tr_tagihan"];
		    $nominal_tr_tagihan = $list_data["nominal_tr_tagihan"];
		    $tgl_inp_tr_tagihan = $list_data["tgl_inp_tr_tagihan"];
		    $sts_pemb = $list_data["sts_pemb"];
		    $vert_pemb_by = $list_data["vert_pemb_by"];
		    $tgl_vert_pemb_by = $list_data["tgl_vert_pemb_by"];
		    $tgl_up_tr_tagihan = $list_data["tgl_up_tr_tagihan"];

		    $nm_bumdes = $list_data["nm_bumdes"];
		    $kd_bumdes = $list_data["kd_bumdes"];
		    $alamat_bumdes = $list_data["alamat_bumdes"];
		    $tarif_bumdes = $list_data["tarif_bumdes"];
		    $username_petugas = $list_data["username_petugas"];
		    $kd_petugas = $list_data["kd_petugas"];
		    $nm_petugas = $list_data["nm_petugas"];
		    $almt_petugas = $list_data["almt_petugas"];
		    $nik_petugas = $list_data["nik_petugas"];
		    $kd_user = $list_data["kd_user"];
		    $nm_user = $list_data["nm_user"];
		    $almt_user = $list_data["almt_user"];
        $disc_user = $list_data["disc_user"];

        $nominal_disc = $disc_user/100 * $nominal_tr_tagihan;
        // $disc_user = "";

		}
	}

?>

<!DOCTYPE html>
<html>
   <head>
      <title>Faktur PDAM</title>
   </head>
   <body>
      <center>
         <table width="100%" border="0" cellpadding="0" cellspacing="0" style='border-collapse: collapse; margin-top: 5px'>
            <td align='center'style='vertical-align:top'>	
               <span style='font-family:"Arial"; font-size:20pt'><b>BUKTI PEMBAYARAN TAGIHAN PDAM</b></span>
               </br><span style='font-family:"Trebuchet MS"; font-size:14pt'><?=$alamat_bumdes?></span>
               </br><span style='font-family:"Trebuchet MS"; font-size:14pt'><?=$nm_bumdes?></span>	
            </td>
         </table>
         <!-- <table style='width:1200px; font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            </table> -->
         <table width="100%" border="0" cellpadding="0" cellspacing="0" style='border-collapse: collapse; margin-top: -10px;'>
            <tr>
               <td align="left">
                  <span style='font-family:"Trebuchet MS"; font-size:12pt'>Kode Admin&nbsp; : <?=$vert_pemb_by?></span>
               </td>
               <td align="right">
                  <span style='font-family:"Trebuchet MS"; font-size:12pt'>Tgl Pembayaran&nbsp; : <?=date("d-m-Y", strtotime($tgl_vert_pemb_by))?></span>
               </td>
            </tr>
            <!-- <tr style="height: 5px;">
               <td colspan="2">
               	<b></b>
               </td>
               </tr> -->
         </table>
         <hr size='2px' color='black' style="margin-top: 0px; margin-bottom: 70px;">
         <!-- <table style='width:60%; font-family:calibri; border-collapse: collapse;' border = '0' >
            </table> -->
         <table width="95%" border="0" cellpadding="0" cellspacing="1" style='border-collapse: collapse; margin-top: 0px;'>
            <tr>
               <td align="left" valign="top" style="width: 50%;">
                  <table border="0" width="100%" style='border-collapse: collapse;'>
                     <tr>
                        <td width="35%"><span style='font-family:"Arial"; font-size:14pt'>Kode Pembayaran</span></td>
                        <td width="3%">:</td>
                        <td width="62%"><b><span style='font-family:"Trebuchet MS"; font-size:14pt'><?=strtoupper($id_tr_tagihan)?></b></span></td>
                     </tr>
                     <tr>
                        <td><span style='font-family:"Arial"; font-size:14pt'>No Pelanggan</span></td>
                        <td>:</td>
                        <td><span style='font-family:"Trebuchet MS"; font-size:14pt'><?=strtoupper($kd_user)?></span></td>
                     </tr>
                     <tr>
                        <td><span style='font-family:"Arial"; font-size:14pt'>Nama</span></td>
                        <td>:</td>
                        <td><span style='font-family:"Trebuchet MS"; font-size:14pt'><b><?=strtoupper($nm_user)?></b></span></td>
                     </tr>
                     <tr>
                        <td><span style='font-family:"Arial"; font-size:14pt'>Lokasi BUMDES</span></td>
                        <td>:</td>
                        <td><span style='font-family:"Trebuchet MS"; font-size:14pt'><b><?=strtoupper($nm_bumdes)?></b></span></td>
                     </tr>
                     <tr>
                        <td><span style='font-family:"Arial"; font-size:14pt'>Alamat</span></td>
                        <td>:</td>
                        <td><span style='font-family:"Trebuchet MS"; font-size:14pt'><?=strtoupper($alamat_bumdes)?></span></td>
                     </tr>
                  </table>
               </td>
               <td align="right" valign="top" style="padding-right:1px; width: 50%;">
                  <table border="0" width="100%" style='border-collapse: collapse;'>
                     <tr>
                        <td width="35%"><span style='font-family:"Arial"; font-size:14pt'>Periode</span></td>
                        <td width="3%">:</td>
                        <td width="62%" align="right"><b><span style='font-family:"Trebuchet MS"; font-size:14pt'><?=date("m-Y", strtotime($periode_tr_tagihan))?></b></span></td>
                     </tr>
                     <tr>
                        <td><span style='font-family:"Arial"; font-size:14pt'>Total Meter</span></td>
                        <td>:</td>
                        <td align="right"><b><span style='font-family:"Trebuchet MS"; font-size:14pt'><?=number_format($meter_tr_tagihan,2, '.', ',')?></b></span></td>
                     </tr>
                     <tr>
                        <td><span style='font-family:"Arial"; font-size:14pt'>Tarif Per Meter</span></td>
                        <td>:</td>
                        <td align="right"><span style='font-family:"Trebuchet MS"; font-size:14pt'><b>Rp. <?=number_format($tarif_bumdes,2, '.', ',')?></b></span></td>
                     </tr>
                     <tr>
                        <td><span style='font-family:"Arial"; font-size:14pt'>Potongan</span></td>
                        <td>:</td>
                        <td align="right"><b><span style='font-family:"Trebuchet MS"; font-size:14pt'>(Potongan Harga <?=$disc_user?>) <br>Rp. <?=number_format($nominal_disc,2, '.', ',')?></span></b></td>
                     </tr>
                     <tr>
                        <td colspan="3"><hr></td>
                     </tr>
                     <!-- <tr>
                        <td><span style='font-family:"Arial"; font-size:14pt'>Lokasi BUMDES</span></td>
                        <td>:</td>
                        <td><span style='font-family:"Trebuchet MS"; font-size:14pt'><b><?=strtoupper($nm_bumdes)?></b></span></td>
                     </tr>
                     <tr>
                        <td><span style='font-family:"Arial"; font-size:14pt'>Alamat</span></td>
                        <td>:</td>
                        <td><span style='font-family:"Trebuchet MS"; font-size:14pt'><?=strtoupper($alamat_bumdes)?></span></td>
                     </tr> -->
                  </table>
               </td>
            </tr>
            <tr>
            	<td></td>
            	<td>
            		<table border="0" width="100%" align="left" style='border-collapse: collapse;'>
				         <tr>
				            <td width="35%"><span style='font-family:"Arial"; font-size:19pt'>Total</span></td>
				            <td width="3%">:</td>
				            <td width="62%" align="right"><b><span style='font-family:"Trebuchet MS"; font-size:19pt'>Rp. <?=number_format($nominal_tr_tagihan, 2, '.', ',')?></b></span></td>
				         </tr>
    				    </table>
    				  </td>
            </tr>
            <tr>
              <td colspan="3" align="center">
                <br><br><br>
                <table border="0" width="50%" align="center" style='border-collapse: collapse;'>
                  <tr>
                    <td><hr></td>
                  </tr>
                  <tr>
                    <td>
                      <span><center>Terimakasih atas pembayaran yang telah dilakukan</center></span>
                      <span><center><?=$nm_bumdes?></center></span>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
         </table>
         <!-- <table style='width:60%; font-size:8pt; font-family:calibri; border-collapse: collapse; ' border = '0'>
            </table> -->
         <br>
         
      </center>
   </body>
   <script type="text/javascript">
      // window.print();
   </script>
</html>