<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-rounded btn-success" data-toggle="modal" data-target="#insert_admin"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah <?=$title?></button>
    </div>
    <!-- <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div> -->
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<?php
    $kd_bumdes_main_str = "";
    if(isset($kd_bumdes_main)){
        if($kd_bumdes_main){
            $kd_bumdes_main_str = $kd_bumdes_main;
        }
    }


    $val_kd_bumdes = $this->uri->segment(3);

    if($val_kd_bumdes == "0" or $val_kd_bumdes == ""){
        $val_kd_bumdes = "0";
        // print_r($val_tgl_start);
    }

    $val_tgl_start = $this->uri->segment(4);
    
    if($val_tgl_start == "0" or $val_tgl_start == ""){
        $val_tgl_start = date("Y-m-d");
        // print_r($val_tgl_start);
    }

    $val_tgl_selesai = $this->uri->segment(5);
    // print_r($val_tgl_selesai);
    if($val_tgl_selesai == "0" or $val_tgl_selesai == ""){
        $val_tgl_selesai = date("Y-m-d");
    }
?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Filter <?=$title?></h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">BUMDES<span style="color: red;">*</span></label>
                                <!-- <input type="text" class="form-control" id="kd_bumdes" name="kd_bumdes" required=""> -->
                                <select class="select2 form-control custom-select" style="width: 100%;" id="kd_bumdes" name="kd_bumdes">
                                    <option value="0">-- All --</option>
                                    <?php
                                        if(isset($list_bumdes)){
                                            if($list_bumdes){
                                                foreach ($list_bumdes as $key => $value) {
                                                    if($value->kd_bumdes != "XX"){
                                                        print_r("<option value=\"".hash('sha256', $value->id_bumdes)."\">(".$value->kd_bumdes.") ".$value->nm_bumdes."</option>");
                                                    }
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                                <p id="msg_kd_bumdes" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Tgl. Mulai<span style="color: red;">*</span></label>
                                <input type="date" class="form-control" id="tgl_mulai" name="tgl_mulai" value="<?=$val_tgl_start?>" required="">
                                <p id="msg_tgl_mulai" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Tgl. Finish <span style="color: red;">*</span></label>
                                <input type="date" class="form-control" id="tgl_selesai" name="tgl_selesai" value="<?=$val_tgl_selesai?>" required="">
                                <p id="msg_tgl_selesai" style="color: red;"></p>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" id="filter" class="btn waves-effect waves-light btn-rounded btn-info">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
        $str_tbl = "";
        $str_t_pemasukan = 0;
        if(isset($tr_tagihan)){
            if($tr_tagihan){
                $no = 1;
                foreach ($tr_tagihan as $key => $val) {
                    $str_sts = "<span class=\"label label-danger\">Belum Terbayar</span>";

                    if($val->sts_pemb == "1"){
                        $str_sts = "<span class=\"label label-info\">Terbayar</span>";
                    }
                   

                    $str_tbl .= "<tr>
                            <td>".$no."</td>
                            <td>(".$val->id_user.") ".$val->nm_user."</td>
                            <td>(".$val->id_petugas.") ".$val->nm_petugas."</td>
                            <td>".$val->tgl_tr_tagihan."</td>
                            <td>".$val->periode_tr_tagihan."</td>

                            <td align=\"right\">".number_format($val->meter_tr_tagihan)."</td>
                            <td align=\"right\">Rp. ".number_format($val->permeter_tr_tagihan, 2, '.', ',')."</td>
                            <td align=\"right\">Rp. ".number_format($val->nominal_tr_tagihan, 2, '.', ',')."<br>$str_sts</td>
                        </tr>";
                    $str_t_pemasukan += $val->nominal_tr_tagihan;
                    $no++;
                }
            }
        }
    ?>
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-b-0 text-white">Tabel <?=$title?></h4>  
                        </div>
                        <div class="col-md-6 text-right">
                            <h4 class="m-b-0 text-white">Rp. <?=number_format($str_t_pemasukan, 2, ',', '.')?></h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="15%">User</th>
                                    <th width="15%">Petugas</th>
                                    <th width="15%">Tgl. Tagihan</th>
                                    <th width="10%">Periode</th>
                                    <th width="10%">Meter</th>
                                    <th width="15%">Tarif Permeter</th>
                                    <th width="15%">Tagihan</th>
                                </tr>
                            </thead>

                            <tbody id="main_table_content">
                                <?=$str_tbl?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <!-- <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div> -->
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->



<script src="<?php print_r(base_url()."assets/js/numeral.min.js");?>"></script>
<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

        $(document).ready(function(){
            $('#kd_bumdes').val("<?=$val_kd_bumdes?>"); 
            $('#kd_bumdes').trigger('change');
        });
    
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#filter").click(function() {
            var tgl_mulai = $("#tgl_mulai").val();
            var tgl_selesai = $("#tgl_selesai").val();
            var kd_bumdes = $("#kd_bumdes").val();

            window.location.href = "<?=base_url()?>admin/laporan_pemasukan/"+kd_bumdes+"/"+tgl_mulai+"/"+tgl_selesai; 
        });
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    

  
</script>