<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->auth_v0->check_session_active_super();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="icon" type="image/png" sizes="16x16" href="<?php print_r(base_url());?>assets/img/logo.png">
    <title>App PDAM</title>
    
</head>

<body style="font-size: 12px;">
	<?php

		$str_basic_id = "";
		if(isset($basic_id)){
			if($basic_id){
				$str_basic_id = $basic_id;
			}
		}

		$id_bumdes = "";
		$kd_user = "";
		$nm_user = "";
		if(isset($list_user)){
			if($list_user){
				$id_bumdes = $list_user["id_bumdes"];
				$kd_user = $list_user["kd_user"];
				$nm_user = $list_user["nm_user"];
			}
		}
	?>

	<table width="90%" align="center" border="1" style="border-collapse: collapse;">
		<tr>
			<td align="center" width="50%"><img style="width:70%; height: auto;" src="<?=base_url()?>assets/qr/<?=$str_basic_id?>.png"></td>
			<td align="center" width="30%" style="vertical-align: bottom;"><h1><?=str_replace(".", "", $kd_user)?></h1></td>
		</tr>
		<tr>
			<td colspan="2" align="center" style="vertical-align: bottom;"><h1><?=strtoupper($nm_user)?></h1></td>
		</tr>
	</table>
</body>

</html>