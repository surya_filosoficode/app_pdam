<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-rounded btn-success" data-toggle="modal" data-target="#insert_admin"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah <?=$title?></button>
    </div>
    <!-- <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div> -->
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel <?=$title?></h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="15%">Nama</th>
                                    <th width="15%">Username</th>
                                    <th width="10%">BUMDES</th>
                                    <th width="15%">Kode Petugas</th>
                                    <th width="15%">Alamat</th>
                                    <th width="10%">Password</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>

                            <tbody id="main_table_content">
                                <?php
                                    if($list_petugas){
                                        $no = 1;
                                        foreach ($list_petugas as $key => $val) {
                                            $str_btn_ch_pass = "<a href=\"javascript:void(0)\" onclick=\"ch_pass('".$val->id_petugas."')\"><span class=\"label label-primary\">UBAH PASSWORD</span></a>";

                                            echo "<tr>
                                                    <td>".$no."</td>
                                                    <td>".$val->nm_petugas."</td>
                                                    <td>".$val->username_petugas."</td>
                                                    <td>(".$val->kd_bumdes.") ".$val->nm_bumdes."</td>
                                                    <td>".$val->kd_petugas."</td>
                                                    <td>".$val->almt_petugas."</td>
                                                    <td>".$str_btn_ch_pass."</td>
                                                    <td>
                                                        <button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$val->id_petugas."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                        <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$val->id_petugas."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                        </center>
                                                    </td>
                                                </tr>";
                                            $no++;
                                        }
                                    }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" id="change_pass_modal" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Ubah Password Petuhas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Password :</label>
                                <input type="Password" class="form-control" id="ch_pass" name="pass" required="">
                                <p id="_msg_ch_pass" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password :</label>
                                <input type="Password" class="form-control" id="ch_repass" name="repass" required="">
                                <p id="_msg_ch_repass" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="add_pass_new" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="insert_admin" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!-- <form method="post" action="<?= base_url()."admin_super/superadmin/insert_admin";?>"> -->
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah <?=$title?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">BUMDES<span style="color: red;">*</span></label>
                                <!-- <input type="text" class="form-control" id="kd_bumdes" name="kd_bumdes" required=""> -->
                                <select class="select2 form-control custom-select" style="width: 100%;" id="kd_bumdes" name="kd_bumdes">
                                    <?php
                                        $arr_data_bumdes = [];
                                        if(isset($list_bumdes)){
                                            if($list_bumdes){
                                                foreach ($list_bumdes as $key => $value) {
                                                    print_r("<option value=\"".$value->id_bumdes."\">(".$value->kd_bumdes.") ".$value->nm_bumdes."</option>");

                                                    $arr_data_bumdes[$value->id_bumdes] = $value->kd_bumdes;
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                                <p id="msg_kd_bumdes" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Petugas<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nm_petugas" name="nm_petugas" required="">
                                <p id="msg_nm_petugas" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">NIK Petugas <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nik_petugas" name="nik_petugas" required="">
                                <p id="msg_nik_petugas" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Username Petugas <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="username" name="username" required="">
                                <p id="msg_username" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Kode Petugas <span style="color: red;">*</span></label>
                                <!-- <input type="text" class="form-control" id="kd_admin" name="kd_admin" required=""> -->
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic_kd_petugas">03</span>
                                    </div>
                                    <input type="number" class="form-control" aria-label="Username" id="kd_petugas" name="kd_petugas" required="">
                                </div>
                                <p id="msg_kd_petugas" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat Petugas <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="almt_petugas" name="almt_petugas" required="">
                                <p id="msg_almt_petugas" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Password <span style="color: red;">*</span></label>
                                <input type="Password" class="form-control" id="pass" name="pass" required="">
                                <p id="msg_pass" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password <span style="color: red;">*</span></label>
                                <input type="Password" class="form-control" id="repass" name="repass" required="">
                                <p id="msg_repass" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>     
            </div>

            <div class="modal-footer">
                <button type="submit" id="add_admin" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah <?=$title?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/update_data";?>" method="post"> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">BUMDES<span style="color: red;">*</span></label>
                                <!-- <input type="text" class="form-control" id="kd_bumdes" name="kd_bumdes" required=""> -->
                                <select class="select2 form-control custom-select" style="width: 100%;" id="_kd_bumdes" name="kd_bumdes">
                                    <?php
                                        if(isset($list_bumdes)){
                                            if($list_bumdes){
                                                foreach ($list_bumdes as $key => $value) {
                                                    print_r("<option value=\"".$value->id_bumdes."\">(".$value->kd_bumdes.") ".$value->nm_bumdes."</option>");
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                                <p id="_msg_kd_bumdes" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Petugas<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nm_petugas" name="nm_petugas" required="">
                                <p id="_msg_nm_petugas" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">NIK Petugas <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nik_petugas" name="nik_petugas" required="">
                                <p id="_msg_nik_petugas" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Username Petugas <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_username" name="username" required="">
                                <p id="_msg_username" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Kode Petugas <span style="color: red;">*</span></label>
                                <!-- <input type="text" class="form-control" id="kd_petugas" name="kd_petugas" required=""> -->
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="_basic_kd_petugas">03</span>
                                    </div>
                                    <input type="text" class="form-control" aria-label="Username" id="_kd_petugas" name="kd_petugas" required="" readonly="">
                                </div>
                                <p id="_msg_kd_petugas" style="color: red;"></p>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat Petugas <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_almt_petugas" name="almt_petugas" required="">
                                <p id="_msg_almt_petugas" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_data" class="btn waves-effect waves-light btn-rounded btn-info">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->


<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

    var arr_data_bumdes = JSON.parse('<?=json_encode($arr_data_bumdes)?>');

    //=========================================================================//
    //-----------------------------------kd_bumdes-----------------------------//
    //=========================================================================//
        $("#kd_bumdes").change(function(){
            var kd_bumdes = $("#kd_bumdes").val();
            $("#basic_kd_petugas").html("03"+arr_data_bumdes[kd_bumdes]);
        });

        $("#_kd_bumdes").change(function(){
            $("#basic_kd_petugas").html();
        });
    //=========================================================================//
    //-----------------------------------kd_bumdes-----------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#add_admin").click(function() {
            var data_main = new FormData();
            data_main.append('kd_bumdes'    , $("#kd_bumdes").val());
            data_main.append('username'     , $("#username").val());
            data_main.append('kd_petugas'   , $("#kd_petugas").val());
            data_main.append('nm_petugas'   , $("#nm_petugas").val());
            data_main.append('almt_petugas' , $("#almt_petugas").val());
            data_main.append('nik_petugas'  , $("#nik_petugas").val());
            data_main.append('password'     , $("#pass").val());
            data_main.append('repassword'   , $("#repass").val());
           
            $.ajax({
                url: "<?php echo base_url()."admin/petugasmain/insert";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_admin').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/petugas");?>");
            } else {
                $("#msg_kd_bumdes").html(detail_msg.kd_bumdes);
                $("#msg_username").html(detail_msg.username);
                $("#msg_kd_petugas").html(detail_msg.kd_petugas);
                $("#msg_nm_petugas").html(detail_msg.nm_petugas);
                $("#msg_almt_petugas").html(detail_msg.almt_petugas);
                $("#msg_nik_petugas").html(detail_msg.nik_petugas);
                $("#msg_pass").html(detail_msg.password);
                $("#msg_repass").html(detail_msg.repassword);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            $("#kd_bumdes").val("");
            $("#username").val("");
            $("#kd_petugas").val("");
            $("#nm_petugas").val("");
            $("#almt_petugas").val("");
            $("#nik_petugas").val("");
            $("#pass").val("");
            $("#repass").val("");
            
            $("#msg_kd_bumdes").html("");
            $("#msg_username").html("");
            $("#msg_kd_petugas").html("");
            $("#msg_nm_petugas").html("");
            $("#msg_almt_petugas").html("");
            $("#msg_nik_petugas").html("");
            $("#msg_pass").html("");
            $("#msg_repass").html("");
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_nm_petugas").val("");
            $("#_username").val("");
            $("#_almt_petugas").val("");
            $("#_kd_bumdes").val("");
            $("#_nik_petugas").val("");

            $("#_msg_kd_bumdes").html("");
            $("#_msg_username").html("");
            $("#_msg_nm_petugas").html("");
            $("#_msg_almt_petugas").html("");
            $("#_msg_nik_petugas").html("");
        }

        function update_data(id_data) {
            clear_form_update();
            // console.log(id_data);

            var data_main = new FormData();
            data_main.append('id_petugas', id_data);

            $.ajax({
                url: "<?php echo base_url()."admin/petugasmain/get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    set_val_update(res, id_data);
                    $("#update_data").modal('show');
                }
            });
        }

        function set_val_update(res, id_data) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_data;

                var kd_petugas = list_data.kd_petugas;
                    var kd_primary = kd_petugas.split(".")[0];
                    var kd_second = kd_petugas.split(".")[1];

                $("#_kd_petugas").val(kd_second);
                $("#_basic_kd_petugas").html(kd_primary);

                // console.log(list_data.id_bumdes);
                $('#_kd_bumdes').val(list_data.id_bumdes); // Select the option with a value of '1'
                $('#_kd_bumdes').trigger('change');

                // $("#_kd_bumdes").val("'"+list_data.id_bumdes+"'");
                $("#_nm_petugas").val(list_data.nm_petugas);
                $("#_username").val(list_data.username_petugas);
                $("#_almt_petugas").val(list_data.almt_petugas);
                $("#_nik_petugas").val(list_data.nik_petugas);
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_data--------------------------//
    //=========================================================================//
        $("#btn_update_data").click(function() {
            var data_main = new FormData();
            data_main.append('id_petugas', id_cache);

            data_main.append('kd_bumdes'      , $("#_kd_bumdes").val());
            data_main.append('username'    , $("#_username").val());
            data_main.append('nm_petugas'     , $("#_nm_petugas").val());
            data_main.append('almt_petugas' , $("#_almt_petugas").val());
            data_main.append('nik_petugas'    , $("#_nik_petugas").val());

            $.ajax({
                url: "<?php echo base_url()."admin/petugasmain/update";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#update_data').modal('toggle');
                clear_form_update();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/petugas");?>");
            } else {
                $("#_msg_kd_bumdes").html(detail_msg.kd_bumdes);
                $("#_msg_username").html(detail_msg.username);
                $("#_msg_nm_petugas").html(detail_msg.nm_petugas);
                $("#_msg_almt_petugas").html(detail_msg.almt_petugas);
                $("#_msg_nik_petugas").html(detail_msg.nik_petugas);
            
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_data--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//

        function method_delete(id_data){
            var data_main = new FormData();
            data_main.append('id_petugas', id_data);

            $.ajax({
                url: "<?php echo base_url()."admin/petugasmain/delete";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_data(id_data) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_data);
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }



        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/petugas");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//


    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//
        $("#add_pass_new").click(function() {
            var data_main = new FormData();
            data_main.append('id_data', id_ch_cache);

            data_main.append('password'     , $("#ch_pass").val());
            data_main.append('repassword'   , $("#ch_repass").val());

            $.ajax({
                url: "<?php echo base_url()."admin/petugasmain/change_pass";?>",
                dataType: 'html',
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_pass_new(res);
                    // response_change_pass(res, id_data_op);
                }
            });
        });

        function response_pass_new(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/petugas");?>");

                clear_ch_pass();
                $('#change_pass_modal').modal('toggle');;
            } else {
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");

                $("#_msg_ch_pass").val(detail_msg.ch_pass);
                $("#_msg_ch_repass").val(detail_msg.ch_repass);
            }
        }

        function ch_pass(id_data) {
            $('#change_pass_modal').modal('show');
            id_ch_cache = id_data;
        }

        function clear_ch_pass(){
            $("#ch_pass").val("");
            $("#ch_repass").val("");
        }
    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//

  
</script>