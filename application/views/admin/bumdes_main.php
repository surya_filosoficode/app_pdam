<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-rounded btn-success" data-toggle="modal" data-target="#insert_admin"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah <?=$title?></button>
    </div>
    <!-- <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div> -->
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel <?=$title?></h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="25%">Nama BUMDES</th>
                                    <th width="15%">Kode BUMDES</th>
                                    <th width="25%">Alamat BUMDES</th>
                                    <th width="15%">Tarif BUMDES</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>

                            <tbody id="main_table_content">
                                <?php
                                    if(!empty($list_bumdes)){
                                        $no = 1;
                                        foreach ($list_bumdes as $key => $val) {
                                            echo "<tr>
                                                    <td>".$no."</td>
                                                    <td>".$val->nm_bumdes."</td>
                                                    <td>".$val->kd_bumdes."</td>
                                                    <td>".$val->alamat_bumdes."</td>
                                                    <td align=\"right\">Rp. ".number_format($val->tarif_bumdes, 2, ',', '.')."</td>
                                                    <td align=\"center\">
                                                        <button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$val->id_bumdes."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                        <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$val->id_bumdes."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                        </center>
                                                    </td>
                                                </tr>";
                                            $no++;
                                        }
                                    }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="insert_admin" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!-- <form method="post" action="<?= base_url()."admin_super/superadmin/insert_admin";?>"> -->
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah <?=$title?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Kode BUMDES <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="kd_bumdes" name="kd_bumdes" required="">
                                <p id="msg_kd_bumdes" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Nama BUMDES <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nm_bumdes" name="nm_bumdes" required="">
                                <p id="msg_nm_bumdes" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Tarif Per Meter BUMDES <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="tarif_bumdes" name="tarif_bumdes" required="">
                                <p id="msg_tarif_bumdes" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Tarif Per Meter BUMDES (RP.)<span style="color: red;">*</span></label>
                                <br>
                                <h3 id="tarif_bumdes_rp">Rp. 0</h3>
                            </div>
                        </div>                        

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat BUMDES <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="alamat_bumdes" name="alamat_bumdes" required="">
                                <p id="msg_alamat_bumdes" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>

            <div class="modal-footer">
                <button type="submit" id="add_admin" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah <?=$title?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/update_data";?>" method="post"> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Kode BUMDES <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_kd_bumdes" name="kd_bumdes" required="" readonly="">
                                <p id="_msg_kd_bumdes" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Nama BUMDES <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nm_bumdes" name="nm_bumdes" required="">
                                <p id="_msg_nm_bumdes" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Tarif Per Meter BUMDES <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="_tarif_bumdes" name="tarif_bumdes" required="">
                                <p id="_msg_tarif_bumdes" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Tarif Per Meter BUMDES (RP.)<span style="color: red;">*</span></label>
                                <br>
                                <h3 id="_tarif_bumdes_rp">Rp. 0</h3>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat BUMDES <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_alamat_bumdes" name="alamat_bumdes" required="">
                                <p id="_msg_alamat_bumdes" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_data" class="btn waves-effect waves-light btn-rounded btn-info">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->

<script src="<?php print_r(base_url()."assets/js/numeral.min.js");?>"></script>
<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

    //=========================================================================//
    //-----------------------------------ch_nominal_inp------------------------//
    //=========================================================================//
        function ch_nominal_inp(){
            var nominal_inp = $("#tarif_bumdes").val();
            $("#tarif_bumdes_rp").html("Rp. "+numeral(nominal_inp).format('0,0'));
        }

        function _ch_nominal_inp(){
            var nominal_inp = $("#_tarif_bumdes").val();
            $("#_tarif_bumdes_rp").html("Rp. "+numeral(nominal_inp).format('0,0'));
        }

        $("#tarif_bumdes").keyup(function(){
            ch_nominal_inp();
        });

        $("#_tarif_bumdes").keyup(function(){
            _ch_nominal_inp();
        });
    //=========================================================================//
    //-----------------------------------ch_nominal_inp------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#add_admin").click(function() {
            var data_main = new FormData();
            data_main.append('kd_bumdes'    , $("#kd_bumdes").val());
            data_main.append('nm_bumdes'    , $("#nm_bumdes").val());
            data_main.append('tarif_bumdes'    , $("#tarif_bumdes").val());
            data_main.append('alamat_bumdes', $("#alamat_bumdes").val());
           
            $.ajax({
                url: "<?php echo base_url()."admin/bumdesmain/insert";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_admin').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/bumdes");?>");
            } else {
                $("#msg_kd_bumdes").html(detail_msg.kd_bumdes);
                $("#msg_nm_bumdes").html(detail_msg.nm_bumdes);
                $("#msg_tarif_bumdes").html(detail_msg.tarif_bumdes);
                $("#msg_alamat_bumdes").html(detail_msg.alamat_bumdes);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            $("#kd_bumdes").val("");
            $("#nm_bumdes").val("");
            $("#tarif_bumdes").val("0");
            $("#alamat_bumdes").val("");
            
            $("#msg_kd_bumdes").html("");
            $("#msg_nm_bumdes").html("");
            $("#msg_tarif_bumdes").html("");
            $("#msg_alamat_bumdes").html("");
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_nm_bumdes").val("");
            $("#_alamat_bumdes").val("");
            $("#_kd_bumdes").val("");
            $("#_tarif_bumdes").val("");

            $("#_msg_kd_bumdes").html("");
            $("#_msg_nm_bumdes").html("");
            $("#_msg_alamat_bumdes").html("");
            $("#_msg_tarif_bumdes").html("");
        }

        function update_data(id_data) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_bumdes', id_data);

            $.ajax({
                url: "<?php echo base_url()."admin/bumdesmain/get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_data);
                    $("#update_data").modal('show');
                }
            });
        }

        function set_val_update(res, id_data) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_data;

                $("#_kd_bumdes").val(list_data.kd_bumdes);
                $("#_nm_bumdes").val(list_data.nm_bumdes);
                $("#_tarif_bumdes").val(list_data.tarif_bumdes);
                $("#_alamat_bumdes").val(list_data.alamat_bumdes);
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_data--------------------------//
    //=========================================================================//
        $("#btn_update_data").click(function() {
            var data_main = new FormData();
            data_main.append('id_bumdes', id_cache);

            data_main.append('kd_bumdes', $("#_kd_bumdes").val());
            data_main.append('nm_bumdes'        , $("#_nm_bumdes").val());
            data_main.append('tarif_bumdes'    , $("#_tarif_bumdes").val());
            data_main.append('alamat_bumdes'     , $("#_alamat_bumdes").val());

            $.ajax({
                url: "<?php echo base_url()."admin/bumdesmain/update";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#update_data').modal('toggle');
                clear_form_update();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/bumdes");?>");
            } else {
                $("#_msg_kd_bumdes").html(detail_msg.kd_bumdes);
                $("#_msg_nm_bumdes").html(detail_msg.nm_bumdes);
                $("#_msg_tarif_bumdes").html(detail_msg.tarif_bumdes);
                $("#_msg_alamat_bumdes").html(detail_msg.alamat_bumdes);
            
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_data--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//

        function method_delete(id_data){
            var data_main = new FormData();
            data_main.append('id_bumdes', id_data);

            $.ajax({
                url: "<?php echo base_url()."admin/bumdesmain/delete";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_data(id_data) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_data);
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }



        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/bumdes");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

  
</script>