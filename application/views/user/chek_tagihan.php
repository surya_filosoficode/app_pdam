<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php print_r(base_url());?>assets/img/ic_logo_water-web.png">
    <title>SipamSimas - Periksa Tagihan</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php print_r(base_url());?>assets/template/main/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php print_r(base_url());?>assets/template/main/css/colors/blue.css" id="theme" rel="stylesheet">
    <!--alerts CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/skins/all.css" rel="stylesheet">

    <script src="<?=base_url()?>assets/js/jquery-3.2.1.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="container-fluid" style="margin-top: 30px;">
            <!-- Row -->
            <div class="row">
                <div class="col-md-12">

                    <div class="col-lg-12 text-center">
                        <h1 class="m-t-30">FORM Periksa Tagihan PDAM</h1>
                        <h5 class="text-muted m-b-30"><i class="ti-pin"></i></h5>
                    </div>
                
                </div>
            </div>
            <!-- Row -->

            <br><br>
            <!-- Row -->
            <div class="row">
                <div class="col-md-2">
                    &nbsp;
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form class="form-horizontal form-material" id="loginform" method="post" action="javascript:void(0)">
                                        <h5 class="box-title m-b-20">
                                            <label style="color: red;">*** </label>
                                            Masukkan Kode Pelanggan dan Nama Pelanggan (5 digit awal)
                                        </h5>
                                        <br>
                                        <div class="form-group m-t-40">
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" name="kd_pelanggan" id="kd_pelanggan" required="" placeholder="Kode Pelanggan (Tanpa titik)">
                                                <p id="msg_kd_pelanggan" style="color: red;font-size: small;"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" name="nama_pelanggan" id="nama_pelanggan" required="" placeholder="Nama Pelanggan (5 digit awal)">
                                                <p id="msg_nama_pelanggan" style="color: red;font-size: small;"></p>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group text-center m-t-20">
                                            <div class="col-xs-12">
                                                <button class="btn btn-rounded btn-info" type="submit" name="login" id="login">Periksa Tagihan</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row -->

            <!-- Row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Daftar Tagihan Pelanggan</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover earning-box">
                                    <thead>
                                        <tr>
                                            <th width="25%">Periode</th>
                                            <th width="15%">Total Meter</th>
                                            <th width="15%">Tarif Permeter</th>
                                            <th width="15%">Diskon</th>
                                            <th width="25%">Total Tagihan</th>
                                        </tr>
                                    </thead>
                                    <tbody id="out_list">
                                        <tr>
                                            <td colspan="5">No Record</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/custom.min.js"></script>
    <!-- Sweet-Alert  -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- icheck -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/icheck.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/icheck.init.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
    
    <script src="<?php print_r(base_url()."assets/js/numeral.min.js");?>"></script>
    <!-- <script src="<?php print_r("assets/js/custom/main_custom.js");?>"></script> -->
    <script type="text/javascript">
        var arr_of_m = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        $(document).ready(function() {
            document.getElementById("kd_pelanggan").focus();
        });

        function send(){
            var data_main = $("#loginform").serialize();
            // console.log(data_main);
            $.ajax({
                url: "<?php echo base_url()."user/checktagihan/check";?>",
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: 'application/x-www-form-urlencoded',
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res, textStatus, jQxhr) {
                    
                    response_login(res);
                }
            });
            
        }

        $("#loginform").submit(function(e){
            e.preventDefault();
            send();
            
        });

        $("#login").click(function(e) {
            e.preventDefault();
            send();
        });

        function response_login(res) {
            var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                var tr_data = data_json.tr_data;

                if (main_msg.status) {
                    render_data(tr_data);
                    
                } else {
                    $("#msg_username").html(detail_msg.kd_pelanggan);
                    $("#msg_password").html(detail_msg.pass_nama);
                }
        }

        function render_data(data){
            var str_tbl = "";

            var t_tagihan = 0;
            for (let i in data) {

                var alamat_bumdes   = data[i].alamat_bumdes;
                var almt_user       = data[i].almt_user;
                var disc_tr_tagihan = data[i].disc_tr_tagihan;
                var kd_bumdes       = data[i].kd_bumdes;
                var kd_user         = data[i].kd_user;
                var meter_tr_tagihan = data[i].meter_tr_tagihan;
                var nm_user         = data[i].nm_user;
                var nominal_tr_tagihan  = data[i].nominal_tr_tagihan;
                var periode_tr_tagihan  = data[i].periode_tr_tagihan.split("-");
                var permeter_tr_tagihan = data[i].permeter_tr_tagihan;

                var nominal_disc = parseFloat(disc_tr_tagihan)/100 * (parseFloat(meter_tr_tagihan) * parseFloat(permeter_tr_tagihan));

                var str_m = arr_of_m[parseInt(periode_tr_tagihan[1])];
                var str_th = periode_tr_tagihan[0];

                str_tbl += "<tr>"+
                                "<td>"+str_m+" "+str_th+"</td>"+
                                "<td align=\"right\">"+numeral(meter_tr_tagihan).format('0,0')+"</td>"+
                                "<td align=\"right\">Rp. "+numeral(permeter_tr_tagihan).format('0,0')+"</td>"+
                                "<td align=\"right\">Rp. "+numeral(nominal_disc).format('0,0')+"</td>"+
                                "<td align=\"right\">Rp. "+numeral(nominal_tr_tagihan).format('0,0')+"</td>"+
                            "</tr>";

                t_tagihan += parseFloat(nominal_tr_tagihan);
            }

            str_tbl += "<tr>"+
                            "<td colspan=\"4\" align=\"center\">Total Tagihan</td>"+
                            "<td align=\"right\">"+numeral(t_tagihan).format('0,0')+"</td>"+
                        "</tr>";

            $("#out_list").html(str_tbl);
        }
    </script>
</body>

</html>