<div class="container">
      <form class="form-signin" accept="utf-8" action="<?php echo base_url() ?>contact">
        <h2 class="form-signin-heading">Please sign in</h2>

        <div class="form-group">
          <label for="inputEmail" class="sr-only">Username</label>
          <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukkan Username Anda" value="<?php echo set_value('username') ?>"  autocomplete="off">
          <?php echo form_error('username'); ?>
        </div>

        <div class="form-group">
          <label for="inputPassword" class="sr-only">Password</label>
          <input type="password" name="nama" class="form-control" id="nama" placeholder="Masukkan Username Anda" value="<?php echo set_value('password') ?>"  autocomplete="off">
          <?php echo form_error('password'); ?>
        </div>

        <div class="form-group">
          <label>Recaptcha</label>
          <?php echo $recaptcha_html;?>
          <?php echo form_error('g-recaptcha-response'); ?>
        </div>

        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
    </div> 
