            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Create Article</h3>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header"><h5>Pilih Image</h5></div>
                            <div class="card-body">
                                
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="row" id="row_img_list">
                    <div class="col-lg-12 col-md-12">
                        <!-- Column -->
                        <div class="card card-default">
                            <div class="card-header">
                                <div class="card-actions">
                                    <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                                    <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                                </div>
                                <h4 class="card-title m-b-0">Image List</h4>
                            </div>
                            <div class="card-body collapse" style="">
                                <div class="col-lg-12 col-md-12">
                                    <div id="slimtest1">
                                        <div class="row el-element-overlay" id="out_img_list"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>           

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5>Pilih Image</h5>
                                    </div>
                                    <!-- <div class="col-md-6 text-right">
                                        <a href="#" onclick="get_image()" style="font-size: 15px;"><i class="mdi mdi-folder-image"></i> &nbsp;&nbsp; Lihat Gambar</a>        
                                    </div> -->
                                </div>
                            </div>
                            <div class="card-body">
                                <?php echo form_open_multipart('admin/postimagemain/save_article');?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Title</label>
                                            <input type="input" class="form-control" name="title" id="title">
                                            <p id="msg_title" style="color: red;"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Article</label>
                                            <textarea class="summernote" name="article" id="article"></textarea>
                                            <p id="msg_article" style="color: red;"></p>
                                        </div>        
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Tag</label><br>
                                            
                                            <select multiple data-role="tagsinput" name="tag" id="tag">
                                            </select>
                                            <p id="msg_tag" style="color: red;"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Categori</label>
                                            
                                            <select class="select2 m-b-10 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose" name="cate" id="cate">
                                                <option value="ok">ok</option>
                                            </select>
                                            <p id="msg_cate" style="color: red;"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">&nbsp;</label>
                                            <br>
                                            <button type="button" name="add_cate" id="add_cate" class="btn btn-info">add</button>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <hr>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <input type="submit" id="save" class="btn btn-success btn-rounded" value="Save">
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->         
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                © 2017 Admin Press Admin by themedesigner.in
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>

    <div class="modal fade bs-example-modal-lg" id="modal_img_detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="d_title_img">Large modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label"><b>Categori/Hastag</b></label>
                                <h6 id="d_category_img"></h6>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <img src="" id="out_base_64" width="100%">  
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

<?php
    if(isset($list_image)){
        if ($list_image) {
            $set_array = [];
            foreach ($list_image as $key => $value) {
                $id_img = $value->id_img;
                $title_img = $value->title_img;
                $path_img = $value->path_img;
                $file_img = $value->file_img;
                $category_img = json_decode($value->category_img);

                $set_array[$id_img] = ["title_img"=>$title_img,
                                        "path_img"=>$path_img,
                                        "file_img"=>$file_img,
                                        "category_img"=>$category_img,
                                    ];
            }

            $set_array = json_encode($set_array);
        }
    }
?>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/summernote/dist/summernote.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/main/js/jquery.slimscroll.js"></script>
    <script src="<?php print_r(base_url());?>assets/js/custom/main_custom.js"></script>
    <script>
    var all_var = "";
    var list_image = JSON.parse('<?php print_r($set_array);?>');



    jQuery(document).ready(function() {

        $('.summernote').summernote({
            height: 350, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });

        $('.inline-editor').summernote({
            airMode: true
        });

        $("#row_img_list").hide(100);

    });

    $('#slimtest1').slimScroll({
        height: '400px'
    });

    $(document).ready(function(){
        render_img(list_image);
    });

    function render_img(data_json){
        // console.log(data_json);
        var str_img = "";
        for (let item in data_json) {
            // console.log(data_json[item]);
            str_img += "<div class=\"col-lg-3 col-md-6\">"+
                            "<div class=\"card\" style=\"margin-bottom: 10px; margin-top: 10px;\">"+
                                "<div class=\"el-card-item\" style=\"padding: 0px;\">"+
                                    "<div class=\"el-card-avatar el-overlay-1\" style=\"margin: 0px;\">"+
                                        "<img src=\"<?php print_r(base_url());?>"+data_json[item]["path_img"]+data_json[item].file_img+"\">"+
                                        "<div class=\"el-overlay\">"+
                                            "<ul class=\"el-info\">"+
                                                "<li><a class=\"btn default btn-outline\" onclick=\"open_image('"+item+"')\"><i class=\"icon-magnifier\"></i></a></li>"+
                                                "<li><a class=\"btn default btn-outline\" href=\"javascript:void(0);\" onclick=\"copy_clip('<?php print_r(base_url());?>"+data_json[item]["path_img"]+data_json[item].file_img+"')\"><i class=\"icon-link\"></i></a></li>"+
                                            "</ul>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>"+
                        "</div>";
        }

        $("#out_img_list").html(str_img);
        $("#row_img_list").show(200);
    }

    function copy_clip(text){
        copyTextToClipboard(text);
    }

    function sip(){
        console.log(all_var);
    }

    function open_image(id_item){
        var main_img     = "<?php print_r(base_url()); ?>"+list_image[id_item].path_img+list_image[id_item].file_img;
        var title_img    = list_image[id_item].title_img;
        var category_img = list_image[id_item].category_img.join(", ");

        $("#d_title_img").html(title_img);
        $("#d_category_img").html(category_img);
        
        $("#out_base_64").attr("src", main_img);

        $("#modal_img_detail").modal("show");
    }

    
    </script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>
