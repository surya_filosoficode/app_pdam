<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->auth_v0->check_session_active_bumd();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php print_r(base_url());?>assets/img/ic_logo_water-web.png">
    <title>SipamSimas - BUMDES Room</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/morrisjs/morris.css" rel="stylesheet">
    <!-- page CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
    <!-- Custom CSS -->
    <link href="<?php print_r(base_url());?>assets/template/main/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php print_r(base_url());?>assets/template/main/css/colors/blue.css" id="theme" rel="stylesheet">

    <!-- toast CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    

    <!--alerts CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/skins/all.css" rel="stylesheet">

    <!-- Cropper CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/cropper/cropper.min.css" rel="stylesheet">

    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">

    <!-- summernotes CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/summernote/dist/summernote.css" rel="stylesheet" />

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
    <script src="<?=base_url()?>assets/js/jquery-3.2.1.js"></script>

    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/jquery/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/template/assets/plugins/datatables/jquery.dataTables.min.js"></script>

    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/cropper/cropper.min.js"></script>

    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>    

    <!-- style="font-size: 14px; color: #000000;" -->
    <style type="text/css">
        .font_edit {
          font-size: 14px;
        }

        .font_color {
          color: #000000;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border" style="font-size: 12px;">
    <?php

        $id_admin = "";
        $id_tipe_admin = "";
        $email = "";
        $username = "";
        $status_active = "";
        $nama_admin = "";
        $nip_admin = "";

        $str_lv = "";


        if($_SESSION["ih_mau_ngapain"]){
            $id_admin       = $_SESSION["ih_mau_ngapain"]["id_admin"];
            $id_tipe_admin  = $_SESSION["ih_mau_ngapain"]["id_tipe_admin"];
            $email          = $_SESSION["ih_mau_ngapain"]["email"];
            $username       = $_SESSION["ih_mau_ngapain"]["username"];
            $status_active  = $_SESSION["ih_mau_ngapain"]["status_active"];
            $nama_admin     = $_SESSION["ih_mau_ngapain"]["nama_admin"];
            $nip_admin      = $_SESSION["ih_mau_ngapain"]["nip_admin"];

            $str_lv = "Admin BUMDES";
            // if($id_tipe_admin == "1"){
            //     $str_lv = "Admin Frontliner";
            // }
        }

    ?>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img width="70%" src="<?php print_r(base_url());?>assets/img/sipamsimas.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php print_r(base_url());?>assets/img/sipamsimas.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <!-- <img src="<?php print_r(base_url());?>assets/template/assets/images/logo-text.png" alt="homepage" class="dark-logo" /> -->
                         <!-- Light Logo text -->    
                         <!-- <img src="<?php print_r(base_url());?>assets/template/assets/images/logo-text.png" class="light-logo" alt="homepage" /> -->
                     </span>
                 </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?php print_r(base_url());?>assets/img/users/icons8.png" alt="user" class="profile-pic" />
                                <?php print_r($str_lv);?>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-text">
                                                <h4><?php print_r($nama_admin);?></h4>
                                                <p class="text-muted"><?php print_r($email);?></p>
                                            </div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<?php print_r(base_url());?>logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="<?php print_r(base_url());?>assets/img/users/icons8.png" alt="user" />
                        <!-- this is blinking heartbit-->
                        <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </div>
                    <!-- User profile text-->
                    <div class="profile-text">
                        <h5><?php print_r($nama_admin);?></h5>
                        <a href="<?php print_r(base_url());?>logout" class="" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
                        
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                    <!-- ============================================================== -->
                    <!-- --------------------------Data Master------------------------- -->
                    <!-- ============================================================== -->
                        <li class="nav-small-cap">DATA MASTER</li>
                        
                        <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-database"></i><span class="hide-menu">Data Master</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>bumdes/bumdes">BUMDES</a></li>
                                <li><a href="<?php print_r(base_url());?>bumdes/petugas">Petugas</a></li>
                                <li><a href="<?php print_r(base_url());?>bumdes/user">User</a></li>
                            </ul>
                        </li>

                        <li><a class="waves-effect waves-dark" href="<?php print_r(base_url());?>bumdes/pengeluaran" aria-expanded="false"><i class="mdi mdi-book-open"></i><span class="hide-menu">Pengeluaran</span></a>
                        </li>

                        <li><a class="waves-effect waves-dark" href="<?php print_r(base_url());?>bumdes/tagihan" aria-expanded="false"><i class="mdi mdi-barcode-scan"></i><span class="hide-menu">Tagihan Pelanggan</span></a>
                        </li>

                        <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-folder-multiple-outline"></i><span class="hide-menu">Laporan</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>bumdes/laporan_pemasukan/0/0">Pemasukan</a></li>
                                <li><a href="<?php print_r(base_url());?>bumdes/laporan_pengeluaran/0/0">Pengeluaran</a></li>
                                <li><a href="<?php print_r(base_url());?>bumdes/laporan_rugi_laba/0/0">Laba Rugi Per BUMDES</a></li>
                            </ul>
                        </li>

                        <!-- <li><a class="waves-effect waves-dark" href="<?php print_r(base_url());?>admin/access" aria-expanded="false"><i class="mdi mdi-access-point"></i><span class="hide-menu">Laporan</span></a></li> -->

                        <!-- <li><a class="waves-effect waves-dark" href="<?php print_r(base_url());?>admin/data_post_article" aria-expanded="false"><i class="mdi mdi-account-key"></i><span class="hide-menu">Article</span></a>
                        </li> -->                        
                    <!-- ============================================================== -->
                    <!-- --------------------------Data Master------------------------- -->
                    <!-- ============================================================== -->
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <?php
            if($page){
                switch ($page) {
                // master
                    case 'petugas_main':
                        include 'admin_bumd/petugas_main.php';
                        break;

                    case 'bumdes_main':
                        include 'admin_bumd/bumdes_main.php';
                        break;

                    case 'user_main':
                        include 'admin_bumd/user_main.php';
                        break;


                // tr_pengeluaran
                    case 'pengeluaran_main':
                        include 'admin_bumd/pengeluaran_main.php';
                        break;

                // tagihan_main
                    case 'tagihan_main':
                        include 'admin_bumd/tagihan_main.php';
                        break;


                // laporan
                    case 'laporan_pengeluaran_main':
                        include 'admin_bumd/laporan_pengeluaran_main.php';
                        break;

                    case 'laporan_pemasukan_main':
                        include 'admin_bumd/laporan_pemasukan_main.php';
                        break;

                    case 'laporan_laba_rugi':
                        include 'admin_bumd/laporan_laba_rugi_new.php';
                        break;


                    default:
                        include 'admin_bumd/admin_main.php';
                        break;
                }
            }
            ?>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019 by filosoficode.com </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--sparkline JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--morris JavaScript -->
    <!-- <script src="<?php print_r(base_url());?>assets/template/assets/plugins/raphael/raphael-min.js"></script> -->
    <!-- <script src="<?php print_r(base_url());?>assets/template/assets/plugins/morrisjs/morris.min.js"></script> -->
    <!-- Chart JS -->
    <!-- <script src="<?php print_r(base_url());?>assets/template/main/js/dashboard1.js"></script> -->

    <!-- Sweet-Alert  -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- icheck -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/icheck.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/icheck.init.js"></script>
    <!-- Toast -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/toast-master/js/jquery.toast.js"></script>
    <!-- This is data table -->
    <script src="<?=base_url()?>assets/template/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <!-- <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script> -->

    <script src="<?php print_r(base_url());?>assets/template/datatable/dataTables.buttons.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/datatable/buttons.flash.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/datatable/jszip.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/datatable/pdfmake.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/datatable/vfs_fonts.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/datatable/buttons.html5.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/datatable/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $('#myTable1').DataTable();
        $('#myTable2').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>


    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/switchery/dist/switchery.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/dff/dff.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php print_r(base_url());?>assets/template/assets/plugins/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript">
    jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
        // For select 2
        $(".select2").select2();
        $('.selectpicker').selectpicker();
        //Bootstrap-TouchSpin
        $(".vertical-spin").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'ti-plus',
            verticaldownclass: 'ti-minus'
        });
        var vspinTrue = $(".vertical-spin").TouchSpin({
            verticalbuttons: true
        });
        if (vspinTrue) {
            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
        }
        $("input[name='tch1']").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '%'
        });
        $("input[name='tch2']").TouchSpin({
            min: -1000000000,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });
        $("input[name='tch3']").TouchSpin();
        $("input[name='tch3_22']").TouchSpin({
            initval: 40
        });
        $("input[name='tch5']").TouchSpin({
            prefix: "pre",
            postfix: "post"
        });
        // For multiselect
        $('#pre-selected-options').multiSelect();
        $('#optgroup').multiSelect({
            selectableOptgroup: true
        });
        $('#public-methods').multiSelect();
        $('#select-all').click(function() {
            $('#public-methods').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function() {
            $('#public-methods').multiSelect('deselect_all');
            return false;
        });
        $('#refresh').on('click', function() {
            $('#public-methods').multiSelect('refresh');
            return false;
        });
        $('#add-option').on('click', function() {
            $('#public-methods').multiSelect('addOption', {
                value: 42,
                text: 'test 42',
                index: 0
            });
            return false;
        });
        $(".ajax").select2({
            ajax: {
                url: "https://api.github.com/search/repositories",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            //templateResult: formatRepo, // omitted for brevity, see the source of this page
            //templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
    });
    </script>

    <!-- <script>
    jQuery(document).ready(function() {

        $('.summernote').summernote({
            height: 350, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });

        $('.inline-editor').summernote({
            airMode: true
        });

    });

    window.edit = function() {
            $(".click2edit").summernote()
        },
        window.save = function() {
            $(".click2edit").summernote('destroy');
        }

    
    </script> -->
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>