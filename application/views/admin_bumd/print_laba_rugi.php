
<?php
    $arr_main = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    $kd_bumdes_main_str = "";
    if(isset($kd_bumdes_main)){
        if($kd_bumdes_main){
            $kd_bumdes_main_str = $kd_bumdes_main;
        }
    }

    $val_tgl_start = $this->uri->segment(3);
    
    if($val_tgl_start == "0" or $val_tgl_start == ""){
        $val_tgl_start = date("Y-m-d");
        // print_r($val_tgl_start);
    }

    $main_date = explode("-", $val_tgl_start);
    $str_tgl_start = $main_date[2]." ".$arr_main[(int)$main_date[1]]." ".$main_date[0];

    $val_tgl_selesai = $this->uri->segment(4);
    // print_r($val_tgl_selesai);
    if($val_tgl_selesai == "0" or $val_tgl_selesai == ""){
        $val_tgl_selesai = date("Y-m-d");
    }

    $main_date = explode("-", $val_tgl_selesai);
    $str_tgl_selesai = $main_date[2]." ".$arr_main[(int)$main_date[1]]." ".$main_date[0];


    $t_all = 0;

    $str_tbl_pemasukan = "";
    $t_list_data_pemasukan = 0;
    if(isset($tr_pemasukan)){
        foreach ($tr_pemasukan as $key => $value) {
            // print_r("<pre>");
            // print_r($value);
            $str_tbl_pemasukan .= "
                    <tr>
                        <td align=\"center\">(".$value->id_tr_tagihan.")</td>
                        <td>Tagihan ".strtoupper($value->nm_user)."</td>
                        <td align=\"center\">:</td>
                        <td align=\"right\">Rp. ".number_format($value->nominal_tr_tagihan,2,',','.')."</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>";

            $t_list_data_pemasukan += $value->nominal_tr_tagihan;          
        }
    }


    $str_tbl_pengeluaran = "";
    $t_list_data_pengeluaran = 0;
    if(isset($tr_pengeluaran)){
        foreach ($tr_pengeluaran as $key => $value) {
            // print_r("<pre>");
            // print_r($value);
            $str_tbl_pengeluaran .= "
                    <tr>
                        <td align=\"center\">(".$value->id_tr_pengeluaran.")</td>
                        <td>".$value->nm_tr_pengeluaran."</td>
                        <td align=\"center\">:</td>
                        <td align=\"right\">Rp. ".number_format($value->nominal_tr_pengeluaran,2,',','.')."</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>";

            $t_list_data_pengeluaran += $value->nominal_tr_pengeluaran;          
        }
    }

    $t_all = $t_list_data_pemasukan - $t_list_data_pengeluaran;
    
?>
    <table width="98%" border="0" align="center">
        <tr>
            <td colspan="7" align="center" style="font-size: 30px;"><b><?=$title?></b></td>
        </tr>
        <tr>
            <td colspan="7" align="center" style="font-size: 20px;">Periode <?=$str_tgl_start?> - <?=$str_tgl_selesai?></td>
        </tr>
        <tr>
            <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
            <td width="20%" align="left"></td>
            <td width="30%"><b></b></td>
            <td width="3%"></td>
            <td width="20%"></td>
            <td width="3%"></td>
            <td></td>
            <td width="3%"></td>
        </tr>
    <!-- Pemasukan -->
        <tr>
            <td colspan="2"><b>1. Pemasukan</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="7">&nbsp;</td>
        </tr>

        <?php
            print_r($str_tbl_pemasukan);
        ?>
        <tr>
            <td></td>
            <td align="center" colspan="3"><hr></td>
            <td align="center">+</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="center"></td>
            <td><b>Jumlah</b></td>
            <td align="center">:</td>
            <td align="right">Rp. <?php print_r(number_format($t_list_data_pemasukan,2,',','.'));?></td>
            <td></td>
            <td align="right">Rp. <?php print_r(number_format($t_list_data_pemasukan,2,',','.'));?></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="7">&nbsp;</td>
        </tr>

    <!-- Pemasukan -->

        <tr>
            <td colspan="7">&nbsp;</td>
        </tr>

    <!-- Pengeluaran -->
        <tr>
            <td colspan="2"><b>2. Pengeluaran</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="7">&nbsp;</td>
        </tr>

        <?php
            print_r($str_tbl_pengeluaran);
        ?>
        <tr>
            <td></td>
            <td align="center" colspan="3"><hr></td>
            <td align="center">+</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="center"></td>
            <td><b>Jumlah</b></td>
            <td align="center">:</td>
            <td align="right">Rp. <?php print_r(number_format($t_list_data_pengeluaran,2,',','.'));?></td>
            <td></td>
            <td align="right">Rp. <?php print_r(number_format($t_list_data_pengeluaran,2,',','.'));?></td>
            <td></td>
        </tr>
    <!-- Pengeluaran -->

        <tr>
            <td colspan="7">&nbsp;</td>
        </tr>

   

        <tr>
            <td></td>
            <td align="center" colspan="3"></td>
            <td align="center"></td>
            <td><hr></td>
            <td align="center">+</td>
        </tr>

        <tr>
            <td></td>
            <td align="center" colspan="3"></td>
            <td align="center"></td>
            <td align="right"><b>Rp. <?php print_r(number_format($t_all,2,',','.'));?></b></td>
            <td align="center"></td>
        </tr>
    </table>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->



<script src="<?php print_r(base_url()."assets/js/numeral.min.js");?>"></script>
<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    window.print();
</script>