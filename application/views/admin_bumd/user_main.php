<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-rounded btn-success" data-toggle="modal" data-target="#insert_admin"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah <?=$title?></button>
    </div>
    <!-- <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div> -->
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<?php
    $kd_bumdes_main_str = "";
    if(isset($kd_bumdes_main)){
        if($kd_bumdes_main){
            $kd_bumdes_main_str = $kd_bumdes_main;
        }
    }
?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel <?=$title?></h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="15%">Nama</th>
                                    <th width="15%">Username</th>
                                    <th width="10%">BUMDES</th>
                                    <th width="15%">Kode User</th>
                                    <th width="15%">Alamat</th>
                                    <th width="10%">Password</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>

                            <tbody id="main_table_content">
                                <?php
                                    if($list_user){
                                        $no = 1;
                                        foreach ($list_user as $key => $val) {
                                            $str_btn_ch_pass = "<a href=\"javascript:void(0)\" onclick=\"ch_pass('".$val->id_user."')\"><span class=\"label label-primary\">UBAH PASSWORD</span></a>";

                                            echo "<tr>
                                                    <td>".$no."</td>
                                                    <td>".$val->nm_user."</td>
                                                    <td>".$val->username_user."</td>
                                                    <td>(".$val->kd_bumdes.") ".$val->nm_bumdes."</td>
                                                    <td>".$val->kd_user."</td>
                                                    <td>".$val->almt_user."</td>
                                                    <td>".$str_btn_ch_pass."</td>
                                                    <td>
                                                        <button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$val->id_user."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                        <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$val->id_user."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>&nbsp;&nbsp;
                                                        <button class=\"btn btn-primary\" id=\"kartu_pelanggan\" onclick=\"get_kartu_pelanggan('".hash('sha256', $val->kd_user)."')\" style=\"width: 40px;\"><i class=\"fa fa-list\"></i></button>
                                                        </center>
                                                    </td>
                                                </tr>";
                                            $no++;
                                        }
                                    }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" id="change_pass_modal" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Ubah Password User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Password :</label>
                                <input type="Password" class="form-control" id="ch_pass" name="pass" required="">
                                <p id="_msg_ch_pass" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password :</label>
                                <input type="Password" class="form-control" id="ch_repass" name="repass" required="">
                                <p id="_msg_ch_repass" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="add_pass_new" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="insert_admin" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!-- <form method="post" action="<?= base_url()."admin_super/superadmin/insert_admin";?>"> -->
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah <?=$title?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                       
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama User<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nm_user" name="nm_user" required="">
                                <p id="msg_nm_user" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">NIK User <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nik_user" name="nik_user" required="">
                                <p id="msg_nik_user" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Username User <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="username" name="username" required="">
                                <p id="msg_username" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Kode User <span style="color: red;">*</span></label>
                                <!-- <input type="text" class="form-control" id="kd_admin" name="kd_admin" required=""> -->
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic_kd_user">01<?=$kd_bumdes_main_str?></span>
                                    </div>
                                    <input type="number" class="form-control" aria-label="Username" id="kd_user" name="kd_user" required="">
                                </div>
                                <p id="msg_kd_user" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Diskon Tagihan (%)<span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="disc_user" name="disc_user" required="">
                                <p id="msg_disc_user" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat User <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="almt_user" name="almt_user" required="">
                                <p id="msg_almt_user" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Password <span style="color: red;">*</span></label>
                                <input type="Password" class="form-control" id="pass" name="pass" required="">
                                <p id="msg_pass" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password <span style="color: red;">*</span></label>
                                <input type="Password" class="form-control" id="repass" name="repass" required="">
                                <p id="msg_repass" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>        
            </div>

            <div class="modal-footer">
                <button type="submit" id="add_admin" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah <?=$title?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/update_data";?>" method="post"> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama User<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nm_user" name="nm_user" required="">
                                <p id="_msg_nm_user" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">NIK User <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nik_user" name="nik_user" required="">
                                <p id="_msg_nik_user" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Username User <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_username" name="username" required="">
                                <p id="_msg_username" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Kode User <span style="color: red;">*</span></label>
                                <!-- <input type="text" class="form-control" id="kd_user" name="kd_user" required=""> -->
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="_basic_kd_user">01<?=$kd_bumdes_main_str?></span>
                                    </div>
                                    <input type="text" class="form-control" aria-label="Username" id="_kd_user" name="kd_user" required="" readonly="">
                                </div>
                                <p id="_msg_kd_user" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Diskon Tagihan (%)<span style="color: red;">*</span></label>
                                <input type="number" step="0.01" class="form-control" id="_disc_user" name="disc_user" required="">
                                <p id="_msg_disc_user" style="color: red;"></p>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat User <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_almt_user" name="almt_user" required="">
                                <p id="_msg_almt_user" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_data" class="btn waves-effect waves-light btn-rounded btn-info">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->


<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

    //=========================================================================//
    //-----------------------------------get_kartu_pelanggan-------------------//
    //=========================================================================//
        function get_kartu_pelanggan(id){
            window.open('<?=base_url()."bumdes/kartu_pelanggan/"?>'+id);
        }
    //=========================================================================//
    //-----------------------------------get_kartu_pelanggan-------------------//
    //=========================================================================//
    

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#add_admin").click(function() {
            var data_main = new FormData();
            // data_main.append('kd_bumdes'    , $("#kd_bumdes").val());
            data_main.append('username'     , $("#username").val());
            data_main.append('kd_user'      , $("#kd_user").val());

            data_main.append('nm_user'      , $("#nm_user").val());
            data_main.append('almt_user'    , $("#almt_user").val());
            data_main.append('nik_user'     , $("#nik_user").val());
            data_main.append('disc_user'     , $("#disc_user").val());

            data_main.append('password'     , $("#pass").val());
            data_main.append('repassword'   , $("#repass").val());
           
            $.ajax({
                url: "<?php echo base_url()."admin_bumd/usermain/insert";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_admin').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."bumdes/user");?>");
            } else {
                // $("#msg_kd_bumdes").html(detail_msg.kd_bumdes);
                $("#msg_username").html(detail_msg.username);
                $("#msg_kd_user").html(detail_msg.kd_user);
                $("#msg_nm_user").html(detail_msg.nm_user);
                $("#msg_almt_user").html(detail_msg.almt_user);
                $("#msg_nik_user").html(detail_msg.nik_user);
                $("#msg_disc_user").html(detail_msg.disc_user);
                $("#msg_pass").html(detail_msg.password);
                $("#msg_repass").html(detail_msg.repassword);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            // $("#kd_bumdes").val("");
            $("#username").val("");
            $("#kd_user").val("");
            $("#nm_user").val("");
            $("#almt_user").val("");
            $("#nik_user").val("");
            $("#disc_user").val("");
            $("#pass").val("");
            $("#repass").val("");
            
            // $("#msg_kd_bumdes").html("");
            $("#msg_kd_user").html("");
            $("#msg_username").html("");
            $("#msg_nm_user").html("");
            $("#msg_almt_user").html("");
            $("#msg_nik_user").html("");
            $("#msg_disc_user").html("");
            $("#msg_pass").html("");
            $("#msg_repass").html("");
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_nm_user").val("");
            $("#_username").val("");
            $("#_almt_user").val("");
            // $("#_kd_bumdes").val("");
            $("#_nik_user").val("");
            $("#_disc_user").val("");

            // $("#_msg_kd_bumdes").html("");
            $("#_msg_username").html("");
            $("#_msg_nm_user").html("");
            $("#_msg_almt_user").html("");
            $("#_msg_nik_user").html("");
            $("#_msg_disc_user").html("");
        }

        function update_data(id_data) {
            clear_form_update();
            // console.log(id_data);

            var data_main = new FormData();
            data_main.append('id_user', id_data);

            $.ajax({
                url: "<?php echo base_url()."admin_bumd/usermain/get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    set_val_update(res, id_data);
                    $("#update_data").modal('show');
                }
            });
        }

        function set_val_update(res, id_data) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_data;

                var kd_user = list_data.kd_user;
                    var kd_primary = kd_user.split(".")[0];
                    var kd_second = kd_user.split(".")[1];

                $("#_kd_user").val(kd_second);
                $("#_basic_kd_user").html(kd_primary);

                // console.log(list_data.id_bumdes);
                // $('#_kd_bumdes').val(list_data.id_bumdes); // Select the option with a value of '1'
                // $('#_kd_bumdes').trigger('change');

                // $("#_kd_bumdes").val("'"+list_data.id_bumdes+"'");
                $("#_nm_user").val(list_data.nm_user);
                $("#_username").val(list_data.username_user);
                $("#_almt_user").val(list_data.almt_user);
                $("#_nik_user").val(list_data.nik_user);
                $("#_disc_user").val(list_data.disc_user);
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_data--------------------------//
    //=========================================================================//
        $("#btn_update_data").click(function() {
            var data_main = new FormData();
            data_main.append('id_user', id_cache);

            // data_main.append('kd_bumdes'      , $("#_kd_bumdes").val());
            data_main.append('username'    , $("#_username").val());
            data_main.append('nm_user'     , $("#_nm_user").val());
            data_main.append('almt_user' , $("#_almt_user").val());
            data_main.append('nik_user'    , $("#_nik_user").val());
            data_main.append('disc_user'    , $("#_disc_user").val());

            $.ajax({
                url: "<?php echo base_url()."admin_bumd/usermain/update";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#update_data').modal('toggle');
                clear_form_update();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."bumdes/user");?>");
            } else {
                // $("#_msg_kd_bumdes").html(detail_msg.kd_bumdes);
                $("#_msg_username").html(detail_msg.username);
                $("#_msg_nm_user").html(detail_msg.nm_user);
                $("#_msg_almt_user").html(detail_msg.almt_user);
                $("#_msg_nik_user").html(detail_msg.nik_user);
                $("#_msg_disc_user").html(detail_msg.disc_user);
            
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_data--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//

        function method_delete(id_data){
            var data_main = new FormData();
            data_main.append('id_user', id_data);

            $.ajax({
                url: "<?php echo base_url()."admin/usermain/delete";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_data(id_data) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_data);
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }



        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."bumdes/user");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//
        $("#add_pass_new").click(function() {
            var data_main = new FormData();
            data_main.append('id_data', id_ch_cache);

            data_main.append('password'     , $("#ch_pass").val());
            data_main.append('repassword'   , $("#ch_repass").val());

            $.ajax({
                url: "<?php echo base_url()."admin/usermain/change_pass";?>",
                dataType: 'html',
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_pass_new(res);
                    // response_change_pass(res, id_data_op);
                }
            });
        });

        function response_pass_new(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."bumdes/user");?>");

                clear_ch_pass();
                $('#change_pass_modal').modal('toggle');;
            } else {
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");

                $("#_msg_ch_pass").val(detail_msg.ch_pass);
                $("#_msg_ch_repass").val(detail_msg.ch_repass);
            }
        }

        function ch_pass(id_data) {
            $('#change_pass_modal').modal('show');
            id_ch_cache = id_data;
        }

        function clear_ch_pass(){
            $("#ch_pass").val("");
            $("#ch_repass").val("");
        }
    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//

  
</script>