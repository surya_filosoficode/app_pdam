<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-rounded btn-success" data-toggle="modal" data-target="#insert_admin"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah <?=$title?></button>
    </div>
    <!-- <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div> -->
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<?php

    $id_user_get = "";
    if(isset($_GET["user"])){
        if($_GET["user"]){
            $id_user_get = $_GET["user"];
        }
    }


    $kd_bumdes_main_str = "";
    if(isset($kd_bumdes_main)){
        if($kd_bumdes_main){
            $kd_bumdes_main_str = $kd_bumdes_main;
        }
    }
?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel User</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="20%">Nama</th>
                                    <th width="15%">Username</th>
                                    <th width="15%">BUMDES</th>
                                    <th width="15%">Kode User</th>
                                    <th width="20%">Alamat</th>
                                    <th width="10%">Aksi</th>
                                </tr>
                            </thead>

                            <tbody id="main_table_content">
                                <?php
                                    if($list_user){
                                        $no = 1;
                                        foreach ($list_user as $key => $val) {
                                           
                                            echo "<tr>
                                                    <td>".$no."</td>
                                                    <td>".$val->nm_user."</td>
                                                    <td>".$val->username_user."</td>
                                                    <td>(".$val->kd_bumdes.") ".$val->nm_bumdes."</td>
                                                    <td>".$val->kd_user."</td>
                                                    <td>".$val->almt_user."</td>
                                                    
                                                    <td align=\"right\">
                                                        <button class=\"btn btn-success\" id=\"get_list\" onclick=\"get_list_data('".$val->id_user."')\" style=\"width: 40px;\"><i class=\"fa fa-list-ol\" ></i></button>
                                                        </center>
                                                    </td>
                                                </tr>";
                                            $no++;
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <a class="btn btn-success" style="width: 40px;"><i class="fa fa-list-ol" style="color: white;"></i></a>
                        <label class="form-label text-success">Periksa Tagihan</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->

    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel <?=$title?></h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="15%">User</th>
                                    <th width="15%">Petugas</th>
                                    <th width="10%">Periode</th>
                                    <th width="10%">Meter</th>
                                    <th width="15%">Tarif Permeter</th>
                                    <th width="15%">Tagihan</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>

                            <tbody id="main_table_content">
                                <?php
                                if(isset($tr_tagihan)){
                                    if($tr_tagihan){
                                        $no = 1;
                                        foreach ($tr_tagihan as $key => $val) {
                                            $str_sts = "<span class=\"label label-danger\">Belum Terbayar</span>";

                                            $str_act = "<button class=\"btn btn-info\" id=\"get_verif\" onclick=\"get_verif_data('".$val->id_tr_tagihan."')\" style=\"width: 40px;\"><i class=\"fa fa-check-square-o\" ></i></button>&nbsp;&nbsp;";

                                            if($val->sts_pemb == "1"){
                                                $str_sts = "<span class=\"label label-info\">Terbayar</span>";

                                                $str_act = "<button class=\"btn btn-primary\" id=\"get_struk\" onclick=\"get_struk_data('".$val->id_tr_tagihan."')\" style=\"width: 40px;\"><i class=\"fa fa-list-alt\"></i></button>";
                                            }
                                           

                                            echo "<tr>
                                                    <td>".$no."</td>
                                                    <td>(".$val->id_user.") ".$val->nm_user."</td>
                                                    <td>(".$val->id_petugas.") ".$val->nm_petugas."</td>
                                                    <td>".$val->periode_tr_tagihan."</td>

                                                    <td align=\"right\">".number_format($val->meter_tr_tagihan)."</td>
                                                    <td align=\"right\">Rp. ".number_format($val->permeter_tr_tagihan, 2, '.', ',')."</td>
                                                    <td align=\"right\">Rp. ".number_format($val->nominal_tr_tagihan, 2, '.', ',')."<br>$str_sts</td>
                                                    <td align=\"right\">
                                                        $str_act
                                                    </td>
                                                </tr>";
                                            $no++;
                                        }
                                    }
                                }
                                    
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <!-- <label class="form-label">Keterangan Tombol Aksi ==> </label> -->

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-check-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Verifikasi Pembayaran</label>,&nbsp;
                        <!-- <a class="btn btn-primary" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a> -->
                        <!-- <label class="form-label text-primary">Struk Pembayaran</label> -->
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->


    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">History <?=$title?></h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="15%">User</th>
                                    <th width="15%">Petugas</th>
                                    <th width="10%">Periode</th>
                                    <th width="10%">Meter</th>
                                    <th width="15%">Tarif Permeter</th>
                                    <th width="15%">Tagihan</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>

                            <tbody id="main_table_content">
                                <?php
                                if(isset($tr_tagihan_ls)){
                                    if($tr_tagihan_ls){
                                        $no = 1;
                                        foreach ($tr_tagihan_ls as $key => $val) {
                                            $str_sts = "<span class=\"label label-danger\">Belum Terbayar</span>";

                                            // $str_act = "<button class=\"btn btn-info\" id=\"get_verif\" onclick=\"get_verif_data('".$val->id_tr_tagihan."')\" style=\"width: 40px;\"><i class=\"fa fa-check-square-o\" ></i></button>&nbsp;&nbsp;";

                                            if($val->sts_pemb == "1"){
                                                $str_sts = "<span class=\"label label-info\">Terbayar</span>";    
                                            }

                                            $str_act = "<button class=\"btn btn-primary\" id=\"get_struk\" onclick=\"get_struk_data('".$val->id_tr_tagihan."')\" style=\"width: 40px;\"><i class=\"fa fa-list-alt\"></i></button>";
                                           

                                            echo "<tr>
                                                    <td>".$no."</td>
                                                    <td>(".$val->id_user.") ".$val->nm_user."</td>
                                                    <td>(".$val->id_petugas.") ".$val->nm_petugas."</td>
                                                    <td>".$val->periode_tr_tagihan."</td>

                                                    <td align=\"right\">".number_format($val->meter_tr_tagihan)."</td>
                                                    <td align=\"right\">Rp. ".number_format($val->permeter_tr_tagihan, 2, '.', ',')."</td>
                                                    <td align=\"right\">Rp. ".number_format($val->nominal_tr_tagihan, 2, '.', ',')."<br>$str_sts</td>
                                                    <td align=\"right\">
                                                        $str_act
                                                    </td>
                                                </tr>";
                                            $no++;
                                        }
                                    }
                                }
                                    
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <!-- <label class="form-label">Keterangan Tombol Aksi ==> </label> -->

                        <!-- <a class="btn btn-info" style="width: 40px;"><i class="fa fa-check-square-o" style="color: white;"></i></a> -->
                        <!-- <label class="form-label text-info">Verifikasi Pembayaran</label>,&nbsp; -->
                        <a class="btn btn-primary" style="width: 40px;"><i class="fa fa-list-alt" style="color: white;"></i></a>
                        <label class="form-label text-primary">Struk Pembayaran</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>



<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="form_verif_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Detail Tagihan Pelanggan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Kode Tagihan<span style="color: red;">*</span></label>
                                    <h5 id="id_tr_tagihan" name="id_tr_tagihan"></h5>
                                    <p id="msg_id_tr_tagihan" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Kode Pelanggan<span style="color: red;">*</span></label>
                                    <h5 id="kd_user" name="kd_user"></h5>
                                    <p id="msg_kd_user" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Nama Pelanggan<span style="color: red;">*</span></label>
                                    <h5 id="nm_user" name="nm_user"></h5>
                                    <p id="msg_nm_user" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Petugas Pencatat <span style="color: red;">*</span></label>
                                    <h5 id="nm_petugas" name="nm_petugas"></h5>
                                    <p id="msg_nm_petugas" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Tgl. Pencatatan <span style="color: red;">*</span></label>
                                    <h5 id="tgl_tr_tagihan" name="tgl_tr_tagihan"></h5>
                                    <p id="msg_tgl_tr_tagihan" style="color: red;"></p>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Periode <span style="color: red;">*</span></label>
                                    <h5 id="periode" name="periode"></h5>
                                    <p id="msg_periode" style="color: red;"></p>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Meter Tagihan<span style="color: red;">*</span></label>
                                    <h5 id="tg_meter" name="tg_meter"></h5>
                                    <p id="msg_tg_meter" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Tarif Per Meter<span style="color: red;">*</span></label>
                                    <h5 id="tg_per_meter" name="tg_per_meter"></h5>
                                    <p id="msg_tg_per_meter" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Potongan Pelanggan<span style="color: red;">*</span></label>
                                    <h5 id="disc_user" name="disc_user"></h5>
                                    <p id="msg_disc_user" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <hr>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <h3 id="xx" name="xx">Total Tagihan</h3>
                                </div>
                            </div>

                            <div class="col-md-6 text-right">
                                <div class="form-group">
                                    <h3 id="tg_total" name="tg_total">RP. 400.000</h3>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" id="cancel_verif" class="btn waves-effect waves-light btn-rounded btn-danger" data-dismiss="#form_verif_data">Batal</button>

                <button type="submit" id="add_verif" class="btn waves-effect waves-light btn-rounded btn-info">Verifikasi Pembayaran</button>
                
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->



<script src="<?php print_r(base_url()."assets/js/numeral.min.js");?>"></script>
<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";
    var id_user_get = "<?=$id_user_get?>";

       
    //=========================================================================//
    //-----------------------------------get_list_data-------------------------//
    //=========================================================================//
        function get_list_data(id_user){
            window.location.href = "<?=base_url();?>bumdes/tagihan/?user="+id_user;
        }
    //=========================================================================//
    //-----------------------------------get_list_data-------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_struk_data-------------------------//
    //=========================================================================//
        function get_struk_data(id_user){
            window.open("<?=base_url();?>struk/strukmain/?id="+id_user, '_blank');
        }
    //=========================================================================//
    //-----------------------------------get_struk_data-------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_verif_data------------------------//
    //=========================================================================//
        function get_verif_data(id_data){
            $("#form_verif_data").modal('show');

            var data_main = new FormData();
            data_main.append('id_data', id_data);

            $.ajax({
                url: "<?php echo base_url()."admin_bumd/tagihanmain/get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_tagihan(res, id_data);
                }
            });
        }

        function set_val_tagihan(res, id_data) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_data;
                $("#id_tr_tagihan").html(list_data.id_tr_tagihan);
                $("#kd_user").html(list_data.kd_user);

                $("#nm_user").html(list_data.nm_user);
                $("#nm_petugas").html(list_data.nm_petugas);
                $("#periode").html(list_data.periode_tr_tagihan);

                $("#tgl_tr_tagihan").html(list_data.tgl_tr_tagihan);

                $("#tg_meter").html(numeral(list_data.meter_tr_tagihan).format('0,0'));
                $("#tg_per_meter").html("Rp. "+numeral(list_data.permeter_tr_tagihan).format('0,0'));
                $("#tg_total").html("Rp. "+numeral(list_data.nominal_tr_tagihan).format('0,0'));

                var meter_tr_tagihan = list_data.meter_tr_tagihan;
                var permeter_tr_tagihan = list_data.permeter_tr_tagihan;
                var disc_tr_tagihan = list_data.disc_tr_tagihan;
                var nominal_disc = (disc_tr_tagihan / 100) * (permeter_tr_tagihan * meter_tr_tagihan);

                $("#disc_user").html("Rp. "+numeral(nominal_disc).format('0,0')+" ( Diskon "+disc_tr_tagihan+"% )");

            }else {
                clear_form_update();
            }
        }

        $("#cancel_verif").click(function(){
            $("#form_verif_data").modal('hide');   
        });
    //=========================================================================//
    //-----------------------------------get_verif_data------------------------//
    //=========================================================================//
    
    //=========================================================================//
    //-----------------------------------acc_verifikasi------------------------//
    //=========================================================================//
        $("#add_verif").click(function(){
            // console.log(id_cache);
            var data_main = new FormData();
            data_main.append('id_data', id_cache);

            $.ajax({
                url: "<?php echo base_url()."admin_bumd/tagihanmain/acc_verif";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    set_verif_tagihan(res);
                }
            });
        });

        function set_verif_tagihan(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."bumdes/tagihan");?>?user="+id_user_get);
                window.open("<?=base_url();?>struk/strukmain/?id="+id_cache, "_blank");
            } else {
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------acc_verifikasi------------------------//
    //=========================================================================//

  
</script>