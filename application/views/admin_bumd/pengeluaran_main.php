<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-rounded btn-success" data-toggle="modal" data-target="#insert_admin"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah <?=$title?></button>
    </div>
    <!-- <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div> -->
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<?php
    $kd_bumdes_main_str = "";
    if(isset($kd_bumdes_main)){
        if($kd_bumdes_main){
            $kd_bumdes_main_str = $kd_bumdes_main;
        }
    }
?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Form <?=$title?></h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Pengeluran<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nm_tr_pengeluaran" name="nm_tr_pengeluaran" required="">
                                <p id="msg_nm_tr_pengeluaran" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Tgl. Pengeluaran <span style="color: red;">*</span></label>
                                <input type="date" class="form-control" id="tgl_tr_pengeluaran" name="tgl_tr_pengeluaran" required="">
                                <p id="msg_tgl_tr_pengeluaran" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nominal <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="nominal_tr_pengeluaran" name="nominal_tr_pengeluaran" required="">
                                <p id="msg_nominal_tr_pengeluaran" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nominal Rp.<span style="color: red;">*</span></label>
                                <h5 id="nominal_tr_pengeluaran_rp" name="nominal_tr_pengeluaran_rp">Rp. </h5>
                                <p id="msg_nominal_tr_pengeluaran_rp" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Keterangan<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="ket_tr_pengeluaran" name="ket_tr_pengeluaran" required="">
                                <p id="msg_ket_tr_pengeluaran" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" id="add_data" class="btn waves-effect waves-light btn-rounded btn-info">Simpan Data</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel <?=$title?></h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="25%">Pengeluaran</th>
                                    <th width="15%">Tgl. Pengeluaran</th>
                                    <th width="15%">Nomial</th>
                                    <th width="25%">Keterangan</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>

                            <tbody id="main_table_content">
                                <?php
                                if(isset($tr_pengeluaran)){
                                    if($tr_pengeluaran){
                                        $no = 1;
                                        foreach ($tr_pengeluaran as $key => $val) {
                                           

                                            echo "<tr>
                                                    <td>".$no."</td>
                                                    <td>".$val->nm_tr_pengeluaran."</td>
                                                    <td>".$val->tgl_tr_pengeluaran."</td>
                                                    <td>Rp. ".number_format($val->nominal_tr_pengeluaran, 2, '.', ',')."</td>
                                                    <td>".$val->ket_tr_pengeluaran."</td>
                                                    <td>
                                                        <button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$val->id_tr_pengeluaran."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                        <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$val->id_tr_pengeluaran."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                    </td>
                                                </tr>";
                                            $no++;
                                        }
                                    }
                                }
                                    
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah <?=$title?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/update_data";?>" method="post"> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Pengeluran<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nm_tr_pengeluaran" name="nm_tr_pengeluaran" required="">
                                <p id="_msg_nm_tr_pengeluaran" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Tgl. Pengeluaran <span style="color: red;">*</span></label>
                                <input type="date" class="form-control" id="_tgl_tr_pengeluaran" name="tgl_tr_pengeluaran" required="">
                                <p id="_msg_tgl_tr_pengeluaran" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nominal <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="_nominal_tr_pengeluaran" name="nominal_tr_pengeluaran" required="">
                                <p id="_msg_nominal_tr_pengeluaran" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nominal Rp.<span style="color: red;">*</span></label>
                                <h5 id="_nominal_tr_pengeluaran_rp" name="nominal_tr_pengeluaran_rp">Rp. </h5>
                                <p id="_msg_nominal_tr_pengeluaran_rp" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Keterangan<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_ket_tr_pengeluaran" name="ket_tr_pengeluaran" required="">
                                <p id="_msg_ket_tr_pengeluaran" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_data" class="btn waves-effect waves-light btn-rounded btn-info">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->
<script src="<?php print_r(base_url()."assets/js/numeral.min.js");?>"></script>
<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

       
    //=========================================================================//
    //-----------------------------------ch_nominal_inp------------------------//
    //=========================================================================//
        function ch_nominal_inp(){
            var nominal_inp = $("#nominal_tr_pengeluaran").val();

            $("#nominal_tr_pengeluaran_rp").html("Rp. "+numeral(nominal_inp).format('0,0'));

            // console.log();
        }

        function _ch_nominal_inp(){
            var nominal_inp = $("#_nominal_tr_pengeluaran").val();

            $("#_nominal_tr_pengeluaran_rp").html("Rp. "+numeral(nominal_inp).format('0,0'));

            console.log(numeral(nominal_inp).format('0,0'));
        }

        $("#nominal_tr_pengeluaran").keyup(function(){
            ch_nominal_inp();
        });

        $("#_nominal_tr_pengeluaran").keyup(function(){
            _ch_nominal_inp();
        });
    //=========================================================================//
    //-----------------------------------ch_nominal_inp------------------------//
    //=========================================================================//
    
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#add_data").click(function() {
            var data_main = new FormData();
            // data_main.append('kd_bumdes'    , $("#kd_bumdes").val());
            data_main.append('nominal_tr_pengeluaran' , $("#nominal_tr_pengeluaran").val());
            data_main.append('nm_tr_pengeluaran'      , $("#nm_tr_pengeluaran").val());
            data_main.append('ket_tr_pengeluaran'     , $("#ket_tr_pengeluaran").val());
            data_main.append('tgl_tr_pengeluaran'     , $("#tgl_tr_pengeluaran").val());

            $.ajax({
                url: "<?php echo base_url()."admin_bumd/mainpengeluaran/insert";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_admin').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."bumdes/pengeluaran");?>");
            } else {
                $("#msg_nominal_tr_pengeluaran").html(detail_msg.nominal_tr_pengeluaran);
                $("#msg_nm_tr_pengeluaran").html(detail_msg.nm_tr_pengeluaran);
                $("#msg_ket_tr_pengeluaran").html(detail_msg.ket_tr_pengeluaran);
                $("#msg_tgl_tr_pengeluaran").html(detail_msg.tgl_tr_pengeluaran);
                
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            // $("#kd_bumdes").val("");
            $("#nominal_tr_pengeluaran").val("");
            $("#nm_tr_pengeluaran").val("");
            $("#ket_tr_pengeluaran").val("");
            $("#tgl_tr_pengeluaran").val("");
            
            // $("#msg_kd_bumdes").html("");
            $("#msg_nominal_tr_pengeluaran").html("");
            $("#msg_nm_tr_pengeluaran").html("");
            $("#msg_ket_tr_pengeluaran").html("");
            $("#msg_tgl_tr_pengeluaran").html("");
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_nm_tr_pengeluaran").val("");
            $("#_nominal_tr_pengeluaran").val("");
            $("#_ket_tr_pengeluaran").val("");
            // $("#_kd_bumdes").val("");
            $("#_tgl_tr_pengeluaran").val("");

            // $("#_msg_kd_bumdes").html("");
            $("#_msg_nominal_tr_pengeluaran").html("");
            $("#_msg_nm_tr_pengeluaran").html("");
            $("#_msg_ket_tr_pengeluaran").html("");
            $("#_msg_tgl_tr_pengeluaran").html("");
        }

        function update_data(id_data) {
            clear_form_update();
            // console.log(id_data);

            var data_main = new FormData();
            data_main.append('id_data', id_data);

            $.ajax({
                url: "<?php echo base_url()."admin_bumd/mainpengeluaran/get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    set_val_update(res, id_data);
                    $("#update_data").modal('show');
                }
            });
        }

        function set_val_update(res, id_data) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_data;

                $("#_nm_tr_pengeluaran").val(list_data.nm_tr_pengeluaran);
                $("#_nominal_tr_pengeluaran").val(list_data.nominal_tr_pengeluaran);
                $("#_nominal_tr_pengeluaran_rp").html("Rp. "+numeral(list_data.nominal_tr_pengeluaran).format('0,0'));

                $("#_ket_tr_pengeluaran").val(list_data.ket_tr_pengeluaran);
                $("#_tgl_tr_pengeluaran").val(list_data.tgl_tr_pengeluaran);
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_data--------------------------//
    //=========================================================================//
        $("#btn_update_data").click(function() {
            var data_main = new FormData();
            data_main.append('id_data', id_cache);

            // data_main.append('kd_bumdes'      , $("#_kd_bumdes").val());
            data_main.append('nominal_tr_pengeluaran' , $("#_nominal_tr_pengeluaran").val());
            data_main.append('nm_tr_pengeluaran'      , $("#_nm_tr_pengeluaran").val());
            data_main.append('ket_tr_pengeluaran'     , $("#_ket_tr_pengeluaran").val());
            data_main.append('tgl_tr_pengeluaran'     , $("#_tgl_tr_pengeluaran").val());

            $.ajax({
                url: "<?php echo base_url()."admin_bumd/mainpengeluaran/update";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#update_data').modal('toggle');
                clear_form_update();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."bumdes/pengeluaran");?>");
            } else {
                // $("#_msg_kd_bumdes").html(detail_msg.kd_bumdes);
                $("#_msg_nominal_tr_pengeluaran").html(detail_msg.nominal_tr_pengeluaran);
                $("#_msg_nm_tr_pengeluaran").html(detail_msg.nm_tr_pengeluaran);
                $("#_msg_ket_tr_pengeluaran").html(detail_msg.ket_tr_pengeluaran);
                $("#_msg_tgl_tr_pengeluaran").html(detail_msg.tgl_tr_pengeluaran);
            
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_data--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//

        function method_delete(id_data){
            var data_main = new FormData();
            data_main.append('id_data', id_data);

            $.ajax({
                url: "<?php echo base_url()."admin_bumd/mainpengeluaran/delete";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_data(id_data) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_data);
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }



        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."bumdes/pengeluaran");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//
        $("#add_pass_new").click(function() {
            var data_main = new FormData();
            data_main.append('id_data', id_ch_cache);

            data_main.append('password'     , $("#ch_pass").val());
            data_main.append('repassword'   , $("#ch_repass").val());

            $.ajax({
                url: "<?php echo base_url()."admin/mainpengeluaran/change_pass";?>",
                dataType: 'html',
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_pass_new(res);
                    // response_change_pass(res, id_data_op);
                }
            });
        });

        function response_pass_new(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."bumdes/user");?>");

                clear_ch_pass();
                $('#change_pass_modal').modal('toggle');;
            } else {
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");

                $("#_msg_ch_pass").val(detail_msg.ch_pass);
                $("#_msg_ch_repass").val(detail_msg.ch_repass);
            }
        }

        function ch_pass(id_data) {
            $('#change_pass_modal').modal('show');
            id_ch_cache = id_data;
        }

        function clear_ch_pass(){
            $("#ch_pass").val("");
            $("#ch_repass").val("");
        }
    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//

  
</script>