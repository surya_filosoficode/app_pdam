<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
    <div class="col-md-6 text-right">
        <!-- <button type="button" class="btn btn-rounded btn-success" data-toggle="modal" data-target="#insert_admin"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah <?=$title?></button> -->
    </div>
    <!-- <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div> -->
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<?php
    $kd_bumdes_main_str = "";
    if(isset($kd_bumdes_main)){
        if($kd_bumdes_main){
            $kd_bumdes_main_str = $kd_bumdes_main;
        }
    }

    $val_tgl_start = $this->uri->segment(3);
    
    if($val_tgl_start == "0" or $val_tgl_start == ""){
        $val_tgl_start = date("Y-m-d");
        // print_r($val_tgl_start);
    }

    $val_tgl_selesai = $this->uri->segment(4);
    // print_r($val_tgl_selesai);
    if($val_tgl_selesai == "0" or $val_tgl_selesai == ""){
        $val_tgl_selesai = date("Y-m-d");
    }
?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Filter <?=$title?></h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Tgl. Mulai<span style="color: red;">*</span></label>
                                <input type="date" class="form-control" id="tgl_mulai" name="tgl_mulai" value="<?=$val_tgl_start?>" required="">
                                <p id="msg_tgl_mulai" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Tgl. Finish <span style="color: red;">*</span></label>
                                <input type="date" class="form-control" id="tgl_selesai" name="tgl_selesai" value="<?=$val_tgl_selesai?>" required="">
                                <p id="msg_tgl_selesai" style="color: red;"></p>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" id="filter" class="btn waves-effect waves-light btn-rounded btn-info">Filter</button>
                            <button type="button" id="print_btn" class="btn waves-effect waves-light btn-rounded btn-primary">Print</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php

        $t_all = 0;

        $str_tbl_pemasukan = "";
        $t_list_data_pemasukan = 0;
        if(isset($tr_pemasukan)){
            foreach ($tr_pemasukan as $key => $value) {
                // print_r("<pre>");
                // print_r($value);
                $str_tbl_pemasukan .= "
                        <tr>
                            <td align=\"center\">".$value->id_tr_tagihan."</td>
                            <td>Tagihan ".strtoupper($value->nm_user)."</td>
                            <td align=\"center\">:</td>
                            <td align=\"right\">Rp. ".number_format($value->nominal_tr_tagihan,2,',','.')."</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>";

                $t_list_data_pemasukan += $value->nominal_tr_tagihan;          
            }
        }


        $str_tbl_pengeluaran = "";
        $t_list_data_pengeluaran = 0;
        if(isset($tr_pengeluaran)){
            foreach ($tr_pengeluaran as $key => $value) {
                // print_r("<pre>");
                // print_r($value);
                $str_tbl_pengeluaran .= "
                        <tr>
                            <td align=\"center\">".$value->id_tr_pengeluaran."</td>
                            <td>".$value->nm_tr_pengeluaran."</td>
                            <td align=\"center\">:</td>
                            <td align=\"right\">Rp. ".number_format($value->nominal_tr_pengeluaran,2,',','.')."</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>";

                $t_list_data_pengeluaran += $value->nominal_tr_pengeluaran;          
            }
        }

        $t_all = $t_list_data_pemasukan - $t_list_data_pengeluaran;
        
    ?>
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-b-0 text-white">Tabel <?=$title?></h4>  
                        </div>
                        <div class="col-md-6 text-right">
                            <h4 class="m-b-0 text-white">Rp. <?php print_r(number_format($t_all,2,',','.'));?></h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <table width="90%" border="0" align="center">
                            <tr>
                                <td width="15%" align="left"></td>
                                <td width="40%"><b></b></td>
                                <td width="3%"></td>
                                <td width="25%"></td>
                                <td width="3%"></td>
                                <td></td>
                                <td width="3%"></td>
                            </tr>
                        <!-- Pemasukan -->
                            <tr>
                                <td colspan="2"><b>1. Pemasukan</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="7">&nbsp;</td>
                            </tr>

                            <?php
                                print_r($str_tbl_pemasukan);
                            ?>
                            <tr>
                                <td></td>
                                <td align="center" colspan="3"><hr></td>
                                <td align="center">+</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="center"></td>
                                <td><b>Jumlah</b></td>
                                <td align="center">:</td>
                                <td align="right">Rp. <?php print_r(number_format($t_list_data_pemasukan,2,',','.'));?></td>
                                <td></td>
                                <td align="right">Rp. <?php print_r(number_format($t_list_data_pemasukan,2,',','.'));?></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="7">&nbsp;</td>
                            </tr>

                        <!-- Pemasukan -->

                            <tr>
                                <td colspan="7">&nbsp;</td>
                            </tr>

                        <!-- Pengeluaran -->
                            <tr>
                                <td colspan="2"><b>3. Pengeluaran</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="7">&nbsp;</td>
                            </tr>

                            <?php
                                print_r($str_tbl_pengeluaran);
                            ?>
                            <tr>
                                <td></td>
                                <td align="center" colspan="3"><hr></td>
                                <td align="center">+</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="center"></td>
                                <td><b>Jumlah</b></td>
                                <td align="center">:</td>
                                <td align="right">Rp. <?php print_r(number_format($t_list_data_pengeluaran,2,',','.'));?></td>
                                <td></td>
                                <td align="right">Rp. <?php print_r(number_format($t_list_data_pengeluaran,2,',','.'));?></td>
                                <td></td>
                            </tr>
                        <!-- Pengeluaran -->

                            <tr>
                                <td colspan="7">&nbsp;</td>
                            </tr>

                       

                            <tr>
                                <td></td>
                                <td align="center" colspan="3"></td>
                                <td align="center"></td>
                                <td><hr></td>
                                <td align="center">+</td>
                            </tr>

                            <tr>
                                <td></td>
                                <td align="center" colspan="3"></td>
                                <td align="center"></td>
                                <td align="right"><b>Rp. <?php print_r(number_format($t_all,2,',','.'));?></b></td>
                                <td align="center"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <!-- <label class="form-label">Keterangan Tombol Aksi ==> </label> -->

                        <!-- <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a> -->
                        <!-- <label class="form-label text-info">Update Data</label>,&nbsp; -->
                        <!-- <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a> -->
                        <!-- <label class="form-label text-danger">Delete Data</label> -->
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->



<script src="<?php print_r(base_url()."assets/js/numeral.min.js");?>"></script>
<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";
    
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#filter").click(function() {
            var tgl_mulai = $("#tgl_mulai").val();
            var tgl_selesai = $("#tgl_selesai").val();

            window.location.href = "<?=base_url()?>bumdes/laporan_rugi_laba/"+tgl_mulai+"/"+tgl_selesai; 
        });
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#print_btn").click(function() {
            var tgl_mulai = $("#tgl_mulai").val();
            var tgl_selesai = $("#tgl_selesai").val();

            window.location.href = "<?=base_url()?>bumdes/print_rugi_laba/"+tgl_mulai+"/"+tgl_selesai; 
        });
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    

  
</script>