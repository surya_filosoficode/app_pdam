<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


#-----------------------------autentication---------------------------
	$route['login'] 		= 'login/loginv0';
	$route['logout'] 		= 'login/logout';
#-----------------------------autentication---------------------------


#-----------------------------master----------------------------------
	$route['admin/data_admin'] 		= 'admin/adminmain';
	
	$route['admin/add_prj'] 		= 'admin/Prjmain/index_add';
	$route['admin/my_prj'] 			= 'admin/projectmain/index';
	$route['admin/ot_prj'] 			= 'admin/usermain/index';

	$route['admin/jn_prj'] 			= 'admin/prjjn/index';
	// $route['admin/jn_prj'] 			= 'admin/usermain/index';

	$route['admin/list_prj'] 		= 'admin/Prjmain/index';
	$route['admin/insert_prj'] 		= 'admin/Prjmain/index_add';
	$route['admin/edit_prj/(:any)'] = 'admin/Prjmain/index_edit/$1';

	$route['admin/detail_prj'] 		= 'admin/Prjmain/detail_add';


	$route['admin/access'] 			= 'admin/accmain/index';

	$route['admin/bumdes'] 			= 'admin/bumdesmain/index';
	$route['admin/petugas'] 		= 'admin/petugasmain/index';
	$route['admin/user'] 			= 'admin/usermain/index';

	$route['admin/kartu_pelanggan/(:any)'] = 'admin/usermain/index_kartu_pelanggan/$1';

	$route['admin/laporan_pengeluaran/(:any)/(:any)/(:any)'] 	= 'admin/mainpengeluaranlaporan/index/$1/$2/$3';
	$route['admin/laporan_pemasukan/(:any)/(:any)/(:any)'] 		= 'admin/mainpemasukanlaporan/index/$1/$2/$3';

	
#-----------------------------master----------------------------------

#-----------------------------admin_bumd----------------------------------

	$route['bumdes/bumdes'] 		= 'admin_bumd/bumdesmain/index';
	$route['bumdes/petugas'] 		= 'admin_bumd/petugasmain/index';
	$route['bumdes/user'] 			= 'admin_bumd/usermain/index';
	$route['bumdes/kartu_pelanggan/(:any)'] = 'admin_bumd/usermain/index_kartu_pelanggan/$1';

	$route['bumdes/pembayaran_pelanggan'] 	= 'admin_bumd/usermain/index';
	$route['bumdes/pengeluaran'] 			= 'admin_bumd/mainpengeluaran/index';
	$route['bumdes/tagihan'] 				= 'admin_bumd/tagihanmain/index';

	$route['bumdes/laporan_pengeluaran/(:any)/(:any)'] 	= 'admin_bumd/mainpengeluaranlaporan/index/$1/$2';
	$route['bumdes/laporan_pemasukan/(:any)/(:any)'] 	= 'admin_bumd/mainpemasukanlaporan/index/$1/$2';
	$route['bumdes/laporan_rugi_laba/(:any)/(:any)'] 	= 'admin_bumd/laporanrugilaba/index/$1/$2';

	$route['bumdes/print_rugi_laba/(:any)/(:any)'] 	= 'admin_bumd/laporanrugilaba/print_index/$1/$2';

	$route['bumdes/laporan_pembayaran_pelanggan'] 	= 'admin_bumd/usermain/index';
	// $route['bumdes/laporan_rugi_laba'] 				= 'admin_bumd/laporanrugilaba/index';

#-----------------------------admin_bumd----------------------------------