<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Store_insert_auto_key extends CI_Model{
    
#===============================================================================
#-----------------------------------admin_insert--------------------------------
#===============================================================================
    public function admin_insert($id_tipe_admin, $email, $username, $password, $nama_admin, $nip_admin){
    	$insert = $this->db->query("SELECT insert_admin('".$id_tipe_admin."', '".$email."', '".$username."', '".$password."', '".$nama_admin."', '".$nip_admin."') AS id_admin");
    	return $insert->row_array();
    }
#===============================================================================
#-----------------------------------admin_insert--------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------user_insert---------------------------------
#===============================================================================
    public function user_insert($id_tipe_user, $email_user, $username, $tlp_user, $nama_user, $alamat_user, $password){
    	$insert = $this->db->query("SELECT insert_user(\"$id_tipe_user\", \"$email_user\", \"$username\", \"$tlp_user\", \"$nama_user\", \"$alamat_user\", \"$password\") AS id_user");
    	return $insert->row_array();
    }
#===============================================================================
#-----------------------------------user_insert---------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------image_insert--------------------------------
#===============================================================================
    public function image_insert($title_img, $path_img, $file_img, $category_img, $date_img, $owner_img, $tipe_owner_img,$jenis_img){
    	$insert = $this->db->query("SELECT insert_image(\"$title_img\", \"$path_img\", \"$file_img\", '$category_img', \"$date_img\", \"$owner_img\", \"$tipe_owner_img\", \"$jenis_img\") AS id_image");
    	return $insert->row_array();
    }
#===============================================================================
#-----------------------------------image_insert--------------------------------
#===============================================================================

    // SELECT fc_insert_m_prj($title_prj, $keterangan_prj, $sumber_prj, $id_jn_prj, $date_start, $date_target, $adm_prj) AS 'id';

#===============================================================================
#-----------------------------------prj_m_insert--------------------------------
#===============================================================================
    public function prj_m_insert($title_prj, $keterangan_prj, $sumber_prj, $id_jn_prj, $date_start, $date_target, $adm_prj){
        $insert = $this->db->query("SELECT fc_insert_m_prj(\"$title_prj\", \"$keterangan_prj\", \"$sumber_prj\", \"$id_jn_prj\", \"$date_start\", \"$date_target\", \"$adm_prj\") AS id");
        return $insert->row_array();
    }
#===============================================================================
#-----------------------------------prj_m_insert--------------------------------
#===============================================================================
}
?>