<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_main extends CI_Model{

	public function select_user($where, $where_or){
		$this->db->select("id_user, id_tipe_user, nama_user, tlp_user, email_user, username, status_active_user, is_delete");

        $this->db->or_where("(username  = '".$where_or["username"]."'");
        $this->db->or_where("email_user = '".$where_or["email_user"]."'");
        $this->db->or_where("tlp_user   = '".$where_or["tlp_user"]."')");
        // $this->db->where($where);

        $data = $this->db->get_where("user", $where)->row_array();
        return $data;
	}

    public function select_user_all($where){
        $this->db->select("id_user, id_tipe_user, nama_user, tlp_user, email_user, username, status_active");
        $data = $this->db->get_where("user", $where)->result();
        return $data;
    }

    public function insert_page_main($id_kategori, $nama_page, $next_page, $waktu, $id_user){
        $data = $this->db->query("select insert_page_main('".$id_kategori."','".$nama_page."','".$next_page."','".$waktu."','".$id_user."') as id_page;");
        return $data;
    }


}
?>