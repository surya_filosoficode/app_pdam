<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Other extends CI_Model{

    # get_data m_prj
    
        public function prj_get_data_all(){
            $this->db->join("jn_prj jp", "mp.id_jn_prj = jp.id_jn_prj");
        	$data = $this->db->get("m_prj mp");
        	return $data->result();
        }

        public function prj_get_data_all_where($where){
            $this->db->join("jn_prj jp", "mp.id_jn_prj = jp.id_jn_prj");
        	$data = $this->db->get_where("m_prj mp", $where);
        	return $data->result();
        }

        public function prj_get_data_each($where){
            $this->db->join("jn_prj jp", "mp.id_jn_prj = jp.id_jn_prj");
        	$data = $this->db->get_where("m_prj mp", $where);
        	return $data->row_array();
        }
    
    # get_admin
    
        public function get_admin_full($where){
            $this->db->join("m_bumdes mb", "ad.id_bumdes = mb.id_bumdes");
            $this->db->order_by("id_admin", "DESC");
            $data = $this->db->get_where("admin ad", $where);
            return $data->result();
        }


    # get_petugas
    
        public function get_petugas_full($where){
            $this->db->join("m_bumdes mb", "ad.id_bumdes = mb.id_bumdes");
            $this->db->order_by("id_petugas", "DESC");
            $data = $this->db->get_where("m_petugas ad", $where);
            return $data->result();
        }

    # get_user
    
        public function get_user_full($where){
            $this->db->join("m_bumdes mb", "ad.id_bumdes = mb.id_bumdes");
            $this->db->order_by("id_user", "DESC");
            $data = $this->db->get_where("m_user ad", $where);
            return $data->result();
        }

    # tr_pengeluaran
    
        public function get_tr_pengeluaran_full($where){
            $this->db->join("m_bumdes mb", "ad.id_bumdes = mb.id_bumdes");
            $this->db->order_by("id_tr_pengeluaran", "DESC");
            $data = $this->db->get_where("tr_pengeluaran ad", $where);
            return $data->result();
        }


    # tr_tagihan
    
        public function get_tr_tagihan_full($where){
            $this->db->join("m_bumdes mb", "th.id_bumdes = mb.id_bumdes");
            $this->db->join("m_petugas mp", "th.id_petugas = mp.id_petugas");
            $this->db->join("m_user mu", "th.id_user = mu.id_user");
            $this->db->order_by("periode_tr_tagihan", "DESC");
            $data = $this->db->get_where("tr_tagihan th", $where);
            return $data->result();
        }

    # tr_tagihan_each
    
        public function get_tr_tagihan_each($where){
            $this->db->join("m_bumdes mb", "th.id_bumdes = mb.id_bumdes");
            $this->db->join("m_petugas mp", "th.id_petugas = mp.id_petugas");
            $this->db->join("m_user mu", "th.id_user = mu.id_user");
            $this->db->order_by("periode_tr_tagihan", "DESC");
            $data = $this->db->get_where("tr_tagihan th", $where);
            return $data->row_array();
        }


    # laporan_pengeluaran
    
        public function get_tr_pengeluaran_report($where, $tgl_start, $tgl_finish){
            $this->db->join("m_bumdes mb", "ad.id_bumdes = mb.id_bumdes");
            $this->db->or_where("ad.tgl_tr_pengeluaran = ", $tgl_finish);
            $this->db->order_by("id_tr_pengeluaran", "DESC");
            $data = $this->db->get_where("tr_pengeluaran ad", $where);
            return $data->result();
        }


    # tr_tagihan_for_api
    
        public function get_tr_tagihan_for_api($where){
            $this->db->join("m_user mu", "th.id_user = mu.id_user");
            $this->db->order_by("periode_tr_tagihan", "DESC");
            $data = $this->db->get_where("tr_tagihan th", $where);
            return $data->result();
        }

    # tagihan_for_user

        public function check_tagihan($where){
            $this->db->select("kd_bumdes, alamat_bumdes, kd_user, nm_user, almt_user, periode_tr_tagihan, permeter_tr_tagihan, meter_tr_tagihan, disc_tr_tagihan, nominal_tr_tagihan");
            $this->db->join("m_bumdes mb", "tt.id_bumdes = mb.id_bumdes");
            $this->db->join("m_user mu", "tt.id_user = mu.id_user");
            $this->db->order_by("tt.periode_tr_tagihan", "DESC");
            return $this->db->get_where("tr_tagihan tt", $where)->result();
        }
}
?>