<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas_main extends CI_Model{

	public function select_petugas($where, $where_or){
		$this->db->select("id_petugas, mb.id_bumdes, nm_bumdes, alamat_bumdes, kd_bumdes, tarif_bumdes, jn_admin, username_petugas, pass_petugas, kd_petugas, nm_petugas, almt_petugas, nik_petugas, is_del_petugas");

        $this->db->or_where("(username_petugas = '".$where_or["username_petugas"]."'");
        $this->db->or_where("kd_petugas = '".$where_or["kd_petugas"]."')");
        // $this->db->where($where);

        $this->db->join("m_bumdes mb", "mp.id_bumdes = mb.id_bumdes");
        $data = $this->db->get_where("m_petugas mp", $where)->row_array();
        return $data;
	}

}
?>