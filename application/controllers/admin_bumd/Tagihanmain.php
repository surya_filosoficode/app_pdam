<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tagihanmain extends CI_Controller {
    public $main_db = "tr_tagihan";
    public $main_index = "index_bumd";

    public $kd_admin;
    public $id_bumdes;
    public $kd_bumdes;

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');
        $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");

        $this->kd_admin = $_SESSION["ih_mau_ngapain"]["kd_admin"];
        $this->id_bumdes = $_SESSION["ih_mau_ngapain"]["id_bumdes"];

        $this->kd_bumdes = $this->mm->get_data_each("m_bumdes", ["id_bumdes"=>$this->id_bumdes])["kd_bumdes"];
        
        // $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
    public function index(){
        $data["page"] = "tagihan_main";
        $data["title"] = "Data Tagihan";

        $data["kd_bumdes_main"] = $this->kd_bumdes;

        $data["tr_tagihan"] = [];
        $data["tr_tagihan_ls"] = [];

        $where_lunas = ["th.id_bumdes"=>$this->id_bumdes];

        $where_tagihan = ["th.id_bumdes"=>$this->id_bumdes];
        if(isset($_GET["user"])){
            if($_GET["user"]){
                $where_lunas["th.id_user"] = $_GET["user"];
                $where_lunas["th.sts_pemb"] = "1";

                $where_tagihan["th.id_user"] = $_GET["user"];
                $where_tagihan["th.sts_pemb"] = "0";

                $data["tr_tagihan"] = $this->ot->get_tr_tagihan_full($where_tagihan);

                $data["tr_tagihan_ls"] = $this->ot->get_tr_tagihan_full($where_lunas);
            }
        }

        $data["list_user"] = $this->ot->get_user_full(array("is_del_user"=>"0", "ad.id_bumdes"=>$this->id_bumdes));
        
        
        
        $this->load->view($this->main_index, $data);
    }
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        $data = [];
        if(isset($_POST["id_data"])){
            $id_tr_tagihan = $this->input->post('id_data');
            $data = $this->ot->get_tr_tagihan_each(array("id_tr_tagihan"=>$id_tr_tagihan));
            if($data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------acc_verif-----------------------------------
#===============================================================================
    public function acc_verif(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("VERIFIKASI_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_data"])){
            $id_tr_tagihan = $this->input->post('id_data');

            $admin          = $this->session->userdata("ih_mau_ngapain")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");

            $set = ["sts_pemb"=>"1",
                    "vert_pemb_by"=>$admin,
                    "tgl_vert_pemb_by"=>$time_update
                ];

            $where = ["id_tr_tagihan"=>$id_tr_tagihan];
            
            $data_tagihan = $this->mm->get_data_each("tr_tagihan", ["id_tr_tagihan"=>$id_tr_tagihan]);
            if($data_tagihan){
                $update = $this->mm->update_data("tr_tagihan", $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("VERIFIKASI_SUC"));
                }
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------acc_verif-----------------------------------
#===============================================================================



}
