<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bumdesmain extends CI_Controller {
    public $main_db = "m_bumdes";
    public $main_index = "index_bumd";

    public $kd_admin;
    public $id_bumdes;
    public $kd_bumdes;

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');
        $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");

        $this->load->library("Generate_qrcode");

        $this->kd_admin = $_SESSION["ih_mau_ngapain"]["kd_admin"];
        $this->id_bumdes = $_SESSION["ih_mau_ngapain"]["id_bumdes"];

        $this->kd_bumdes = $this->mm->get_data_each("m_bumdes", ["id_bumdes"=>$this->id_bumdes])["kd_bumdes"];
        
        // $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
    public function index(){
        $data["page"] = "bumdes_main";
        $data["title"] = "Data BUMDES";

        $data["kd_bumdes_main"] = $this->kd_bumdes;

        $data["list_bumdes"] = $this->mm->get_data_all_where($this->main_db, array("is_del_bumdes"=>"0", "id_bumdes"=>$this->id_bumdes));
        $this->load->view($this->main_index, $data);
    }
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_admin--------------------------------
#===============================================================================
    public function val_form_insert(){
        $config_val_input = array(
                array(
                    'field'=>'kd_bumdes',
                    'label'=>'kd_bumdes',
                    'rules'=>'required|is_unique['.$this->main_db.'.nm_bumdes]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )  
                ),array(
                    'field'=>'nm_bumdes',
                    'label'=>'nm_bumdes',
                    'rules'=>'required|is_unique['.$this->main_db.'.nm_bumdes]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    ) 
                ),array(
                    'field'=>'alamat_bumdes',
                    'label'=>'alamat_bumdes',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tarif_bumdes',
                    'label'=>'tarif_bumdes',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "kd_bumdes"=>"",
                    "nm_bumdes"=>"",
                    "alamat_bumdes"=>"",
                    "tarif_bumdes"=>""
                );

        
        if($this->val_form_insert()){
            $kd_bumdes      = $this->input->post("kd_bumdes", true);
            $nm_bumdes      = $this->input->post("nm_bumdes", true);
            $alamat_bumdes  = $this->input->post("alamat_bumdes", true);
            $tarif_bumdes  = $this->input->post("tarif_bumdes", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $kd_bumdes],
                                [$type_pattern, $nm_bumdes]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $status_active  = "0";
                $admin_del      = $this->session->userdata("ih_mau_ngapain")["id_admin"];
                $time_update    = date("Y-m-d h:i:s");

                $data = ["id_bumdes"=>"",
                        "kd_bumdes"=>$kd_bumdes,
                        "nm_bumdes"=>$nm_bumdes,
                        "alamat_bumdes"=>$alamat_bumdes,
                        "tarif_bumdes"=>$tarif_bumdes,
                        "is_del_bumdes"=>"0"
                    ];

                // $insert = $this->ma->admin_insert($kd_bumdes, $nm_bumdes, $alamat_bumdes, hash("sha256", $password), $nama_admin, $nip_admin);
                $insert = $this->mm->insert_data($this->main_db, $data);

                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
                
            }
        }else{

            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["kd_bumdes"]= strip_tags(form_error('kd_bumdes'));
            $msg_detail["nm_bumdes"]        = strip_tags(form_error('nm_bumdes'));
            $msg_detail["alamat_bumdes"]    = strip_tags(form_error('alamat_bumdes'));
            $msg_detail["tarif_bumdes"]    = strip_tags(form_error('tarif_bumdes'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_bumdes"])){
            $id_bumdes = $this->input->post('id_bumdes');
            $data = $this->mm->get_data_each($this->main_db, array("id_bumdes"=>$id_bumdes, "is_del_bumdes"=>"0"));
            if($data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'kd_bumdes',
                    'label'=>'kd_bumdes',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )  
                ),array(
                    'field'=>'nm_bumdes',
                    'label'=>'nm_bumdes',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    ) 
                ),array(
                    'field'=>'alamat_bumdes',
                    'label'=>'alamat_bumdes',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tarif_bumdes',
                    'label'=>'tarif_bumdes',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "kd_bumdes"=>"",
                    "nm_bumdes"=>"",
                    "alamat_bumdes"=>"",
                    "tarif_bumdes"=>""
                );

        if($this->val_form_update()){
            $id_bumdes      = $this->input->post("id_bumdes", true);

            $kd_bumdes      = $this->input->post("kd_bumdes", true);
            $nm_bumdes      = $this->input->post("nm_bumdes", true);
            $alamat_bumdes  = $this->input->post("alamat_bumdes", true);
            $tarif_bumdes  = $this->input->post("tarif_bumdes", true);
           

            $status_active  = "0";
            $admin_del      = $this->session->userdata("ih_mau_ngapain")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");

            // check alamat_bumdes
            if(!$this->mm->get_data_each($this->main_db, ["id_bumdes!="=>$id_bumdes, "kd_bumdes"=>$kd_bumdes])){
                // check nm_bumdes 
                if(!$this->mm->get_data_each($this->main_db, ["id_bumdes!="=>$id_bumdes, "nm_bumdes"=>$nm_bumdes])){

                    $type_pattern   = "allowed_general_char";

                    $arr_pattern  = [[$type_pattern, $kd_bumdes],
                                        [$type_pattern, $nm_bumdes]];


                    if($this->magic_pattern->set_list_pattern($arr_pattern)){
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
                    } else{
                        
                        $set = array(
                            "nm_bumdes"=>$nm_bumdes,
                            "alamat_bumdes"=>$alamat_bumdes,
                            "tarif_bumdes"=>$tarif_bumdes
                            
                        );

                        $where = array("id_bumdes"=>$id_bumdes);

                        $update = $this->mm->update_data($this->main_db, $set, $where);
                        if($update){
                            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                        }
                          
                    }
                } else{
                    $msg_detail["nm_bumdes"] = "Nama BUMDES sudah di ter-rekam pada data master";
                }
            }else{
                $msg_detail["kd_bumdes"] = "Kode BUMDES sudah di ter-rekam pada data master";
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["kd_bumdes"]= strip_tags(form_error('kd_bumdes'));
            $msg_detail["nm_bumdes"]        = strip_tags(form_error('nm_bumdes'));
            $msg_detail["alamat_bumdes"]    = strip_tags(form_error('alamat_bumdes'));
            $msg_detail["tarif_bumdes"]    = strip_tags(form_error('tarif_bumdes'));  
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

    public function delete(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_bumdes"=>"",
                );

        if($_POST["id_bumdes"]){
            $id_bumdes = $this->input->post("id_bumdes");
            
            $set = array("is_del_bumdes"=>"1");
            $where = array("id_bumdes"=>$id_bumdes);

            // $delete_admin = $this->mm->delete_data("admin", array("id_bumdes"=>$id_bumdes));
            $delete_admin = $this->mm->update_data($this->main_db, $set, $where);
            
            if($delete_admin){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_bumdes"]= strip_tags(form_error('id_bumdes'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================



}
