<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainpemasukanlaporan extends CI_Controller {
    public $main_db = "tr_tagihan";
    public $main_index = "index_bumd";

    public $kd_admin;
    public $id_bumdes;
    public $kd_bumdes;

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');
        $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");

        $this->kd_admin = $_SESSION["ih_mau_ngapain"]["kd_admin"];
        $this->id_bumdes = $_SESSION["ih_mau_ngapain"]["id_bumdes"];

        $this->kd_bumdes = $this->mm->get_data_each("m_bumdes", ["id_bumdes"=>$this->id_bumdes])["kd_bumdes"];
        
        // $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index($tgl_start = "0", $tgl_finish = "0"){
		$data["page"] = "laporan_pemasukan_main";
        $data["title"] = "Data Pemasukan";

        $data["kd_bumdes_main"] = $this->kd_bumdes;
        
        $data["tr_pemasukan"] = [];
        if($tgl_start != "0" and $tgl_finish != "0"){
            $data["tr_pemasukan"] = $this->ot->get_tr_tagihan_full(["th.id_bumdes"=>$this->id_bumdes,
                                                                    "th.sts_pemb"=>"1",
                                                                    "th.tgl_tr_tagihan >="=>$tgl_start,
                                                                    "th.tgl_tr_tagihan <="=>$tgl_finish]);    
        }
		
        $this->load->view($this->main_index, $data);
	}

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

}
