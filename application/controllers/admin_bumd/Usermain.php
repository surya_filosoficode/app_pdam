<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermain extends CI_Controller {
    public $main_db = "m_user";
    public $main_index = "index_bumd";

    public $kd_admin;
    public $id_bumdes;
    public $kd_bumdes;

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');
        $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");

        $this->load->library("Generate_qrcode");

        $this->kd_admin = $_SESSION["ih_mau_ngapain"]["kd_admin"];
        $this->id_bumdes = $_SESSION["ih_mau_ngapain"]["id_bumdes"];

        $this->kd_bumdes = $this->mm->get_data_each("m_bumdes", ["id_bumdes"=>$this->id_bumdes])["kd_bumdes"];
        
        // $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "user_main";
        $data["title"] = "Data User";

        $data["kd_bumdes_main"] = $this->kd_bumdes;
        
		$data["list_user"] = $this->ot->get_user_full(array("is_del_user"=>"0", "ad.id_bumdes"=>$this->id_bumdes));
        $data["list_bumdes"] = $this->mm->get_data_all_where("m_bumdes", array("is_del_bumdes"=>"0", "id_bumdes"=>$this->id_bumdes));
        $this->load->view($this->main_index, $data);
	}

    public function index_kartu_pelanggan($id = ""){
        $data["list_user"] = [];
        $data["basic_id"] = $id;

        if($id){
            $data["list_user"] = $this->mm->get_data_each("m_user", ["sha2(kd_user, '256') = "=> $id]);
        }


        $this->load->view("other/kartu_pelanggan", $data);
        // print_r($data);
    }
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_admin--------------------------------
#===============================================================================
	public function val_form_insert(){
        $config_val_input = array(
                array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required|is_unique['.$this->main_db.'.username_user]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'kd_user',
                    'label'=>'kd_user',
                    'rules'=>'required|exact_length[7]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nm_user',
                    'label'=>'nm_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'almt_user',
                    'label'=>'almt_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'nik_user',
                    'label'=>'nik_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'disc_user',
                    'label'=>'disc_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repassword',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "username"=>"",
                    "kd_user"=>"",
                    "nm_user"=>"",
                    "almt_user"=>"",
                    "nik_user"=>"",
                    "disc_user"=>"",
                    "password"=>"",
                    "repassword"=>""
                );

        
        if($this->val_form_insert()){
            $username   = $this->input->post("username", true);
            $kd_user    = $this->input->post("kd_user", true);
            
            $nm_user 	= $this->input->post("nm_user", true);
            $almt_user 	= $this->input->post("almt_user");
            $nik_user   = $this->input->post("nik_user", true);
            $disc_user   = $this->input->post("disc_user", true);

            $password   = $this->input->post("password", true);
            $repassword = $this->input->post("repassword", true);

            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nm_user]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $status_active  = "0";
                $admin_del      = $this->session->userdata("ih_mau_ngapain")["id_admin"];
                $time_update    = date("Y-m-d h:i:s");

                if ($password == $repassword) {

                    $kd_user_fix = "01".$this->kd_bumdes.".".$kd_user;

                    $check_kd_user = $this->mm->get_data_each($this->main_db, ["kd_user"=>$kd_user_fix]);
                    if(!$check_kd_user){

                        $data = ["id_user"=>"",
                                "id_bumdes"=>$this->id_bumdes,
                                "jn_admin"=>"3",
                                "username_user"=>$username,
                                "pass_user"=>hash("sha256", $password),
                                "kd_user"=>$kd_user_fix,
                                "nm_user"=>$nm_user,
                                "almt_user"=>$almt_user,
                                "nik_user"=>$nik_user,
                                "disc_user"=>$disc_user,
                                "created_by_user"=>$admin_del,
                                "date_created_user"=>$time_update,
                                "is_del_user"=>"0"
                            ];

                        $file_name = $this->generate_qrcode->get_qr_code(hash('sha256', $kd_user_fix));
                        if($file_name){
                            if(file_exists("./assets/qr/".$file_name.".png")){
                                $insert = $this->mm->insert_data($this->main_db, $data);

                                if($insert){
                                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                                }
                            }
                        }
                    }else{
                        $msg_detail["kd_user"]   = "kode petugas sudah digunakan";
                    }
                }else{
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
                    
                }
            }
        }else{

            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["username"]   = strip_tags(form_error('username'));
            $msg_detail["kd_user"]   = strip_tags(form_error('kd_user'));
            $msg_detail["nm_user"]   = strip_tags(form_error('nm_user'));
            $msg_detail["almt_user"] = strip_tags(form_error('almt_user'));
            $msg_detail["nik_user"]  = strip_tags(form_error('nik_user'));
            $msg_detail["disc_user"]  = strip_tags(form_error('disc_user'));
            $msg_detail["password"]  = strip_tags(form_error('password'));
            $msg_detail["repassword"]  = strip_tags(form_error('repassword'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_user"])){
        	$id_user = $this->input->post('id_user');
        	$data = $this->mm->get_data_each($this->main_db, array("id_user"=>$id_user, "is_del_user"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'nm_user',
                    'label'=>'nm_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'almt_user',
                    'label'=>'almt_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'nik_user',
                    'label'=>'nik_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'disc_user',
                    'label'=>'disc_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "username"=>"",
                    "nm_user"=>"",
                    "almt_user"=>"",
                    "nik_user"=>"",
                    "disc_user"=>""
                );

        if($this->val_form_update()){
        	$id_user 	= $this->input->post("id_user", true);
            $username 	    = $this->input->post("username", true);
            $nm_user 	= $this->input->post("nm_user", true);
            $almt_user 	= $this->input->post("almt_user", true);
            $nik_user    = $this->input->post("nik_user", true);
            $disc_user   = $this->input->post("disc_user", true);

            $status_active 	= "0";
            $admin_del 		= $this->session->userdata("ih_mau_ngapain")["id_admin"];
            $time_update 	= date("Y-m-d h:i:s");

          	

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nm_user],
                                [$type_pattern, $nik_user]];


            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                
                $set = array(
                    "username_user"=>$username,
                    "nm_user"=>$nm_user,
                    "almt_user"=>$almt_user,
                    "nik_user"=>$nik_user,
                    "disc_user"=>$disc_user
                );

                $where = array("id_user"=>$id_user);

                $update = $this->mm->update_data($this->main_db, $set, $where);
                // print_r($this->db->last_query());
                // die();
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
                  
            }

        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["username"] = strip_tags(form_error('username'));
            $msg_detail["nm_user"]       = strip_tags(form_error('nm_user'));
            $msg_detail["almt_user"] 	= strip_tags(form_error('almt_user'));
            $msg_detail["nik_user"]      = strip_tags(form_error('nik_user'));       
            $msg_detail["disc_user"]  = strip_tags(form_error('disc_user')); 
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

    public function delete(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                );

        if($_POST["id_user"]){
        	$id_user = $this->input->post("id_user");
        	
            $set = array("is_del_user"=>"1");
            $where = array("id_user"=>$id_user);

        	// $delete_admin = $this->mm->delete_data("admin", array("id_user"=>$id_user));
        	$delete_admin = $this->mm->update_data($this->main_db, $set, $where);
            
            if($delete_admin){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_user"]= strip_tags(form_error('id_user'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------change_password-----------------------------
#===============================================================================
    public function val_form_ch_pass(){
        $config_val_input = array(
                array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repassword',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function change_pass(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "password"=>"",
                    "repassword"=>""
                );

        if($this->val_form_ch_pass()){
            $id_data       = $this->input->post("id_data");
            $password       = $this->input->post("password");
            $repassword     = $this->input->post("repassword");

            // check username
            if($password == $repassword){
                $set = array(
                        "pass_user"=>hash("sha256", $password)
                    );

                $where = array("id_user"=>$id_data);

                $update = $this->mm->update_data($this->main_db, $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }else{
                $msg_detail["username"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            
            $msg_detail["password"]     = strip_tags(form_error('password'));
            $msg_detail["repassword"]   = strip_tags(form_error('repassword'));         
        }

        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------change_password-----------------------------
#===============================================================================
}
