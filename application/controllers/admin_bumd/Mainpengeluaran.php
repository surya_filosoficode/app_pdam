<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainpengeluaran extends CI_Controller {
    public $main_db = "tr_pengeluaran";
    public $main_index = "index_bumd";

    public $kd_admin;
    public $id_bumdes;
    public $kd_bumdes;

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');
        $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");

        $this->kd_admin = $_SESSION["ih_mau_ngapain"]["kd_admin"];
        $this->id_bumdes = $_SESSION["ih_mau_ngapain"]["id_bumdes"];

        $this->kd_bumdes = $this->mm->get_data_each("m_bumdes", ["id_bumdes"=>$this->id_bumdes])["kd_bumdes"];
        
        // $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "pengeluaran_main";
        $data["title"] = "Data User";

        $data["kd_bumdes_main"] = $this->kd_bumdes;
        
		$data["tr_pengeluaran"] = $this->ot->get_tr_pengeluaran_full(array("ad.id_bumdes"=>$this->id_bumdes));
        
        $this->load->view($this->main_index, $data);
	}

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_admin--------------------------------
#===============================================================================
	public function val_form_insert(){
        $config_val_input = array(
                array(
                    'field'=>'tgl_tr_pengeluaran',
                    'label'=>'tgl_tr_pengeluaran',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nm_tr_pengeluaran',
                    'label'=>'nm_tr_pengeluaran',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nominal_tr_pengeluaran',
                    'label'=>'nominal_tr_pengeluaran',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'ket_tr_pengeluaran',
                    'label'=>'ket_tr_pengeluaran',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "tgl_tr_pengeluaran"=>"",
                    "nm_tr_pengeluaran"=>"",
                    "nominal_tr_pengeluaran"=>"",
                    "ket_tr_pengeluaran"=>""
                );

        
        if($this->val_form_insert()){
            $tgl_tr_pengeluaran     = $this->input->post("tgl_tr_pengeluaran", true);
            $nm_tr_pengeluaran 	    = $this->input->post("nm_tr_pengeluaran", true);
            $ket_tr_pengeluaran 	= $this->input->post("ket_tr_pengeluaran");
            $nominal_tr_pengeluaran = $this->input->post("nominal_tr_pengeluaran", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nm_tr_pengeluaran]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $status_active  = "0";
                $admin_del      = $this->session->userdata("ih_mau_ngapain")["id_admin"];
                $time_update    = date("Y-m-d h:i:s");

                $data = ["id_tr_pengeluaran"=>"",
                        "id_bumdes"=>$this->id_bumdes,
                        "tgl_tr_pengeluaran"=>$tgl_tr_pengeluaran,
                        "nm_tr_pengeluaran"=>$nm_tr_pengeluaran,
                        "nominal_tr_pengeluaran"=>$nominal_tr_pengeluaran,
                        "ket_tr_pengeluaran"=>$ket_tr_pengeluaran,

                        "tgl_inp_tr_pengeluaran"=>$time_update,
                        "create_by_tr_pengeluaran"=>$admin_del,
                        "tgl_up_tr_pengeluaran"=>$time_update,
                        "up_by_tr_pengeluaran"=>$admin_del
                    ];

               
                $insert = $this->mm->insert_data($this->main_db, $data);

                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
                    
            }
        }else{

            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["tgl_tr_pengeluaran"]   = strip_tags(form_error('tgl_tr_pengeluaran'));
            $msg_detail["nm_tr_pengeluaran"]   = strip_tags(form_error('nm_tr_pengeluaran'));
            $msg_detail["ket_tr_pengeluaran"] = strip_tags(form_error('ket_tr_pengeluaran'));
            $msg_detail["nominal_tr_pengeluaran"]  = strip_tags(form_error('nominal_tr_pengeluaran'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_data"])){
        	$id_tr_pengeluaran = $this->input->post('id_data');
        	$data = $this->mm->get_data_each($this->main_db, array("id_tr_pengeluaran"=>$id_tr_pengeluaran));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

    public function update(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "tgl_tr_pengeluaran"=>"",
                    "nm_tr_pengeluaran"=>"",
                    "nominal_tr_pengeluaran"=>"",
                    "ket_tr_pengeluaran"=>""
                );

        if($this->val_form_insert()){
        	$id_tr_pengeluaran 	= $this->input->post("id_data", true);

            $tgl_tr_pengeluaran     = $this->input->post("tgl_tr_pengeluaran", true);
            $nm_tr_pengeluaran      = $this->input->post("nm_tr_pengeluaran", true);
            $ket_tr_pengeluaran     = $this->input->post("ket_tr_pengeluaran");
            $nominal_tr_pengeluaran = $this->input->post("nominal_tr_pengeluaran", true);

            $status_active 	= "0";
            $admin_del 		= $this->session->userdata("ih_mau_ngapain")["id_admin"];
            $time_update 	= date("Y-m-d h:i:s");

          	

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nm_tr_pengeluaran]];


            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                
                $set = array(
                    "tgl_tr_pengeluaran"=>$tgl_tr_pengeluaran,
                    "nm_tr_pengeluaran"=>$nm_tr_pengeluaran,
                    "ket_tr_pengeluaran"=>$ket_tr_pengeluaran,
                    "nominal_tr_pengeluaran"=>$nominal_tr_pengeluaran,
                );

                $where = array("id_tr_pengeluaran"=>$id_tr_pengeluaran);

                $update = $this->mm->update_data($this->main_db, $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
                  
            }

        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["tgl_tr_pengeluaran"]   = strip_tags(form_error('tgl_tr_pengeluaran'));
            $msg_detail["nm_tr_pengeluaran"]   = strip_tags(form_error('nm_tr_pengeluaran'));
            $msg_detail["ket_tr_pengeluaran"] = strip_tags(form_error('ket_tr_pengeluaran'));
            $msg_detail["nominal_tr_pengeluaran"]  = strip_tags(form_error('nominal_tr_pengeluaran'));     
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

    public function delete(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_tr_pengeluaran"=>"",
                );

        if($_POST["id_data"]){
        	$id_tr_pengeluaran = $this->input->post("id_data");
        	
            $where = array("id_tr_pengeluaran"=>$id_tr_pengeluaran);

        	// $delete_admin = $this->mm->delete_data("admin", array("id_tr_pengeluaran"=>$id_tr_pengeluaran));
        	$delete_admin = $this->mm->delete_data($this->main_db, $where);
            
            if($delete_admin){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_tr_pengeluaran"]= strip_tags(form_error('id_tr_pengeluaran'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

}
