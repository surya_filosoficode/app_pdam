<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Strukmain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('other/other', 'ot');
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");

    }

	public function index(){

		$msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL")];
        $msg_detail = ["id_tr_tagihan" => ""];

        $main_data = [];
        $msg_array["list_data"] = [];
        if(isset($_GET["id"])){
        	if($_GET["id"]){
	            $id_tr_tagihan   = $this->input->get('id', true);

	            $main_data = $this->ot->get_tr_tagihan_each(["id_tr_tagihan"=>$id_tr_tagihan]);
	        }else{
	        	$msg_detail["id_tr_tagihan"] = form_error("id_tr_tagihan");
	        }
        }

        // print_r($main_data);

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $msg_array["list_data"] = $main_data;
		$this->load->view("struk/struk_main", $msg_array);
	}

}
