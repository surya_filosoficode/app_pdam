<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        
        $this->load->library("response_message");
        $this->load->library("Auth_v0");

        date_default_timezone_set("Asia/Bangkok");
    }

    public function get_all_image(){
        $list_image = $this->mm->get_data_all_where("m_img", []);

        print_r(json_encode($list_image));
    }

    

}
