<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainpelanggan extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        
        $this->load->library("magic_pattern");
        $this->load->library("response_message");
        $this->load->library("Auth_v0");
    }

	public function index(){
		include APPPATH . 'libraries/qrlib.php';

		$tempdir = "assets/tmp/"; //Nama folder tempat menyimpan file qrcode
		if (!file_exists($tempdir)) //Buat folder bername temp
			mkdir($tempdir);

		//isi qrcode jika di scan
		$codeContents = 't34g5gh6u5786534532523ewfg83g4tgf8ehf0h09h320thf28hhhhhhf0h0293hf094h2g298g9823h89'; 

		//simpan file kedalam folder temp dengan nama 001.png
		QRcode::png($codeContents,$tempdir."test.png", QR_ECLEVEL_L, 10, 1); 


		echo '<h2>Simpan File QRCode</h2>';
		//menampilkan file qrcode 
		echo '<img src="'.base_url()."/".$tempdir.'test.png" />';
	}

	
}
