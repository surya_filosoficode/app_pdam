<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugasmain extends CI_Controller {
    public $main_db = "m_petugas";

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');
        $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        
        // $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "petugas_main";
        $data["title"] = "Data Petugas";

		$data["list_petugas"] = $this->ot->get_petugas_full(array("is_del_petugas"=>"0"));
        $data["list_bumdes"] = $this->mm->get_data_all_where("m_bumdes", array("is_del_bumdes"=>"0"));
        $this->load->view('index', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_admin--------------------------------
#===============================================================================
	public function val_form_insert(){
        $config_val_input = array(
                array(
                    'field'=>'kd_bumdes',
                    'label'=>'kd_bumdes',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required|is_unique['.$this->main_db.'.username_petugas]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'kd_petugas',
                    'label'=>'kd_petugas',
                    'rules'=>'required|exact_length[7]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nm_petugas',
                    'label'=>'nm_petugas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'almt_petugas',
                    'label'=>'almt_petugas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'nik_petugas',
                    'label'=>'nik_petugas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repassword',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "kd_bumdes"=>"",
                    "username"=>"",
                    "kd_petugas"=>"",
                    "nm_petugas"=>"",
                    "almt_petugas"=>"",
                    "nik_petugas"=>"",
                    "password"=>"",
                    "repassword"=>""
                );

        
        if($this->val_form_insert()){
            $kd_bumdes 	        = $this->input->post("kd_bumdes", true);
            $username           = $this->input->post("username", true);
            $kd_petugas         = $this->input->post("kd_petugas", true);
            $nm_petugas 		= $this->input->post("nm_petugas", true);
            $almt_petugas 	    = $this->input->post("almt_petugas", true);
            $nik_petugas        = $this->input->post("nik_petugas", true);
            $password           = $this->input->post("password", true);
            $repassword         = $this->input->post("repassword", true);

            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $kd_bumdes],
                                [$type_pattern, $nm_petugas]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $status_active  = "0";
                $admin_del      = $this->session->userdata("ih_mau_ngapain")["id_admin"];
                $time_update    = date("Y-m-d h:i:s");

                if ($password == $repassword) {

                    $get_kd_bumdes = $this->mm->get_data_each("m_bumdes", ["id_bumdes"=>$kd_bumdes]);
                        $kd = $get_kd_bumdes["kd_bumdes"];

                        $kd_petugas_fix = "03".$kd.".".$kd_petugas;

                    $check_kd_petugas = $this->mm->get_data_each($this->main_db, ["kd_petugas"=>$kd_petugas_fix]);
                    if(!$check_kd_petugas){
                        $data = ["id_petugas"=>"",
                                "id_bumdes"=>$kd_bumdes,
                                "jn_admin"=>"2",
                                "username_petugas"=>$username,
                                "pass_petugas"=>hash("sha256", $password),
                                "kd_petugas"=>$kd_petugas_fix,
                                "nm_petugas"=>$nm_petugas,
                                "almt_petugas"=>$almt_petugas,
                                "nik_petugas"=>$nik_petugas,
                                "created_by_petugas"=>$admin_del,
                                "date_created_petugas"=>$time_update,
                                "is_del_petugas"=>"0"
                            ];

                        $insert = $this->mm->insert_data($this->main_db, $data);

                        if($insert){
                            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                        }
                    }else{
                        $msg_detail["kd_petugas"]   = "kode petugas sudah digunakan";
                    }
                }else{
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
                    
                }
                
            }
        }else{

            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["kd_bumdes"]    = strip_tags(form_error('kd_bumdes'));
            $msg_detail["username"]   = strip_tags(form_error('username'));
            $msg_detail["kd_petugas"]   = strip_tags(form_error('kd_petugas'));
            $msg_detail["nm_petugas"]   = strip_tags(form_error('nm_petugas'));
            $msg_detail["almt_petugas"] = strip_tags(form_error('almt_petugas'));
            $msg_detail["nik_petugas"]  = strip_tags(form_error('nik_petugas'));
            $msg_detail["password"]  = strip_tags(form_error('password'));
            $msg_detail["repassword"]  = strip_tags(form_error('repassword'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_petugas"])){
        	$id_petugas = $this->input->post('id_petugas');
        	$data = $this->mm->get_data_each($this->main_db, array("id_petugas"=>$id_petugas, "is_del_petugas"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'kd_bumdes',
                    'label'=>'kd_bumdes',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'nm_petugas',
                    'label'=>'nm_petugas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'almt_petugas',
                    'label'=>'almt_petugas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'nik_petugas',
                    'label'=>'nik_petugas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "kd_bumdes"=>"",
                    "username"=>"",
                    "nm_petugas"=>"",
                    "almt_petugas"=>"",
                    "nik_petugas"=>""
                );

        if($this->val_form_update()){
        	$id_petugas 	= $this->input->post("id_petugas", true);
            $kd_bumdes      = $this->input->post("kd_bumdes", true);
            $username 	    = $this->input->post("username", true);
            $nm_petugas 	= $this->input->post("nm_petugas", true);
            $almt_petugas 	= $this->input->post("almt_petugas", true);
            $nik_petugas    = $this->input->post("nik_petugas", true);

            $status_active 	= "0";
            $admin_del 		= $this->session->userdata("ih_mau_ngapain")["id_admin"];
            $time_update 	= date("Y-m-d h:i:s");

          	

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nm_petugas],
                                [$type_pattern, $nik_petugas]];


            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                
                $set = array(
                    "username_petugas"=>$username,
                    "nm_petugas"=>$nm_petugas,
                    "almt_petugas"=>$almt_petugas,
                    "nik_petugas"=>$nik_petugas,
                );

                $where = array("id_petugas"=>$id_petugas);

                $update = $this->mm->update_data($this->main_db, $set, $where);
                // print_r($this->db->last_query());
                // die();
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
                  
            }

        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["kd_bumdes"]        = strip_tags(form_error('kd_bumdes'));
            $msg_detail["username"] = strip_tags(form_error('username'));
            $msg_detail["nm_petugas"]       = strip_tags(form_error('nm_petugas'));
            $msg_detail["almt_petugas"] 	= strip_tags(form_error('almt_petugas'));
            $msg_detail["nik_petugas"]      = strip_tags(form_error('nik_petugas'));       
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

    public function delete(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_petugas"=>"",
                );

        if($_POST["id_petugas"]){
        	$id_petugas = $this->input->post("id_petugas");
        	
            $set = array("is_del_petugas"=>"1");
            $where = array("id_petugas"=>$id_petugas);

        	// $delete_admin = $this->mm->delete_data("admin", array("id_petugas"=>$id_petugas));
        	$delete_admin = $this->mm->update_data($this->main_db, $set, $where);
            
            if($delete_admin){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_petugas"]= strip_tags(form_error('id_petugas'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------change_password_admin-----------------------
#===============================================================================
    public function val_form_ch_pass(){
        $config_val_input = array(
                array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repassword',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function change_pass(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "password"=>"",
                    "repassword"=>""
                );

        if($this->val_form_ch_pass()){
            $id_data       = $this->input->post("id_data");
            $password       = $this->input->post("password");
            $repassword     = $this->input->post("repassword");

            // check username
            if($password == $repassword){
                $set = array(
                        "pass_petugas"=>hash("sha256", $password)
                    );

                $where = array("id_petugas"=>$id_data);

                $update = $this->mm->update_data($this->main_db, $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }else{
                $msg_detail["username"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            
            $msg_detail["password"]     = strip_tags(form_error('password'));
            $msg_detail["repassword"]   = strip_tags(form_error('repassword'));         
        }

        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------change_password_admin-----------------------
#===============================================================================



}
