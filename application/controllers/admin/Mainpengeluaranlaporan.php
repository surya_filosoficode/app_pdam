<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainpengeluaranlaporan extends CI_Controller {
    public $main_db = "tr_pengeluaran";

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');
        $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");

        $this->load->library("Generate_qrcode");
        
        // $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index($id_bumdes = "0", $tgl_start = "0", $tgl_finish = "0"){
		$data["page"] = "laporan_pengeluaran_main";
        $data["title"] = "Data Pengeluaran";

        $where = [];
        $data["tr_pengeluaran"] = [];

        if($id_bumdes != "0"){
            $where["sha2(ad.id_bumdes, '256') = "] = $id_bumdes;
        }
        
        if($tgl_start != "0" and $tgl_finish != "0"){
            $where["tgl_tr_pengeluaran >= "] = $tgl_start;
            $where["tgl_tr_pengeluaran <= "] = $tgl_finish;   
        }

        $data["tr_pengeluaran"] = $this->ot->get_tr_pengeluaran_full($where);
        // print_r("<pre>");
        // print_r($data);
        // die();
        
        $data["list_bumdes"] = $this->mm->get_data_all_where("m_bumdes", array("is_del_bumdes"=>"0"));

        $this->load->view('index', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
}
