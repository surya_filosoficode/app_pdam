<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checktagihan extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');
        //load library
        // $this->load->library("ReCaptcha");
        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
    }

    public function index()
    {
        // $data = array(
        //     'recaptcha_html' => $this->recaptcha->render()
        // );
        // //set form validation
        // $this->form_validation->set_rules('username', 'Username', 'required');
        // $this->form_validation->set_rules('password', 'Password', 'required');
        // $this->form_validation->set_rules('g-recaptcha-response', '<strong>Captcha</strong>', 'callback_getResponseCaptcha');
        // //set message form validation
        // $this->form_validation->set_message('required', '{field} is required.');
        // $this->form_validation->set_message('callback_getResponseCaptcha', '{field} {g-recaptcha-response} harus diisi. ');
        // if($this->form_validation->run() == TRUE)
        // {

        //     //kondisi jika recaptcha dan form validasi terpenusi

        // }else{
            $this->load->view('user/chek_tagihan');
        // }
    }

    public function getResponseCaptcha($str)
    {
        $this->load->library('recaptcha');
        $response = $this->recaptcha->verifyResponse($str);
        if ($response['success'])
        {
            return true;
        }
        else
        {
            $this->form_validation->set_message('getResponseCaptcha', '%s is required.' );
            return false;
        }
    }

    public function val_form(){
        $config_val_input = array(
                array(
                    'field'=>'kd_pelanggan',
                    'label'=>'kd_pelanggan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nama_pelanggan',
                    'label'=>'nama_pelanggan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function check(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "kd_pelanggan"=>"",
                    "nama_pelanggan"=>""
                );

        $tr_tagihan = [];
        if($this->val_form()){
            $kd_pelanggan   = $this->input->post("kd_pelanggan", true);
            $nama_pelanggan    = $this->input->post("nama_pelanggan", true);

            $chek_user = $this->mm->get_data_each("m_user", ["REPLACE(kd_user, '.', '') = "=>$kd_pelanggan,
                                                            "LEFT(nm_user, 5) = "=>$nama_pelanggan]
                                                );
            if($chek_user){
                $id_user = $chek_user["id_user"];

                $where_tagihan["tt.id_user"] = $id_user;
                $where_tagihan["tt.sts_pemb"] = "0";
                
                $tr_tagihan = $this->ot->check_tagihan($where_tagihan); 
                
                $msg_main = ["status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC")];

            }

        }else{

            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));

            $msg_detail["kd_pelanggan"]   = strip_tags(form_error('kd_pelanggan'));
            $msg_detail["nama_pelanggan"]   = strip_tags(form_error('nama_pelanggan'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        $res_msg["tr_data"] = $tr_tagihan;
        print_r(json_encode($res_msg));
    }
}