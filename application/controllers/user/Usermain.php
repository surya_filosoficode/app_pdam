<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermain extends CI_Controller {
    public $main_db = "m_user";

	public function __construct(){
        parent::__construct(); 
        $this->load->model('user/user_main', 'um');
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');
        $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        
        // $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
    private function val_form_log(){
        $config_val_input = array(
                array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'password',
                    'label'=>'password',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_auth(){
        // print_r($_POST);
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL"));
        $msg_detail = array("username" => "",
                            "password" => "");
        if($this->val_form_log()){
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $where = array(
                    'pass_user' => hash("sha256", $password),
                    "is_del_user" => "0"
                );

            $where_or = array(
                    'kd_user' => $username,
                    'username_user' => $username
                );
            
            $cek = $this->um->select_user($where, $where_or);
            // $cek["status_log"] = false;
            if($cek){
                $cek["status_log"] = true;
                if($this->auth_v0->set_session($cek)){
                    $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("LOG_SUC"));
                }
            }

        }else {
            $msg_detail["username"] = form_error("username");
            $msg_detail["password"] = form_error("password");

            $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        }
        // print_r($cek);

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
	
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================


}
