<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        
        $this->load->library("Auth_v0");     
        // $this->load->library("magic_pattern");
        $this->load->library("Generate_qrcode");

    }

	public function index(){
		redirect("user/checktagihan");
	}

	public function get_ses(){
		print_r($_SESSION);
	}

	public function get_qr_code(){
		$file_name = $this->generate_qrcode->get_qr_code("02A.0000001");
		
	        
	 //    // end displaying
	    echo "<img style=\"width: 500px; height: 500px\" src=\"".base_url()."assets/qr/".$file_name.".png\">";
	 //    echo '<img src="'.EXAMPLE_TMP_URLRELPATH.'006_M.png" />';
	 //    echo '<img src="'.EXAMPLE_TMP_URLRELPATH.'006_Q.png" />';
	 //    echo '<img src="'.EXAMPLE_TMP_URLRELPATH.'006_H.png" />';
	}


	public function cek_qr_code(){
		if(file_exists("./assets/qr/02A.0000001.png")){
			print_r("ok");
		}
	}
}
