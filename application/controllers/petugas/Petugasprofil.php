<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugasprofil extends CI_Controller {
    public $main_db = "m_petugas";
    public $tagihan_db = "tr_tagihan";

    public $GLOBAL_TOKEN;

	public function __construct(){
        parent::__construct(); 
        $this->load->model('petugas/petugas_main', 'um');
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');
        $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        $this->load->library("Globalvar");

        $this->GLOBAL_TOKEN = $this->globalvar->GLOBAL_TOKEN;
        
        // $this->auth_v0->check_session_active_ad();
    }


#===============================================================================
#-----------------------------------update_petugas------------------------------
#===============================================================================

    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'nm_petugas',
                    'label'=>'nm_petugas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'almt_petugas',
                    'label'=>'almt_petugas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'nik_petugas',
                    'label'=>'nik_petugas',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "username"=>"",
                    "nm_petugas"=>"",
                    "almt_petugas"=>"",
                    "nik_petugas"=>"",
                    "token"=>""
                );

        if($this->val_form_update()){
            $id_petugas     = $this->input->post("id_petugas", true);
            $username       = $this->input->post("username", true);
            $nm_petugas     = $this->input->post("nm_petugas", true);
            $almt_petugas   = $this->input->post("almt_petugas", true);
            $nik_petugas    = $this->input->post("nik_petugas", true);
            $token          = $this->input->post("token", true);

            $time_update    = date("Y-m-d h:i:s");

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nm_petugas],
                                [$type_pattern, $nik_petugas]];

            if($token == $this->GLOBAL_TOKEN){
                if($this->magic_pattern->set_list_pattern($arr_pattern)){
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
                } else{
                    
                    $set = array(
                        "username_petugas"=>$username,
                        "nm_petugas"=>$nm_petugas,
                        "almt_petugas"=>$almt_petugas,
                        "nik_petugas"=>$nik_petugas
                    );

                    $where = array("id_petugas"=>$id_petugas);

                    $update = $this->mm->update_data($this->main_db, $set, $where);
                    // print_r($this->db->last_query());
                    // die();
                    if($update){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                      
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["username"]         = strip_tags(form_error('username'));
            $msg_detail["nm_petugas"]       = strip_tags(form_error('nm_petugas'));
            $msg_detail["almt_petugas"]     = strip_tags(form_error('almt_petugas'));
            $msg_detail["nik_petugas"]      = strip_tags(form_error('nik_petugas'));  
            $msg_detail["token"]            = strip_tags(form_error('token'));       
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_petugas------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------change_password_admin-----------------------
#===============================================================================
    public function val_form_ch_pass(){
        $config_val_input = array(
                array(
                    'field'=>'password_old',
                    'label'=>'Password_old',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repassword',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function change_pass(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "password_old"=>"",
                    "password"=>"",
                    "repassword"=>"",
                    "token"=>""
                );

        if($this->val_form_ch_pass()){
            $id_petugas     = $this->input->post("id_petugas");
            $password_old   = $this->input->post("password_old");
            $password       = $this->input->post("password");
            $repassword     = $this->input->post("repassword");
            $token          = $this->input->post("token");

            if($token == $this->GLOBAL_TOKEN){
                // check username
                if($password == $repassword){
                    $check_petugas = $this->mm->get_data_each($this->main_db, ["id_petugas"=>$id_petugas, "pass_petugas"=>hash("sha256", $password_old)]);
                    if($check_petugas){
                        $set = array(
                                "pass_petugas"=>hash("sha256", $password)
                            );

                        $where = array("id_petugas"=>$id_petugas);

                        $update = $this->mm->update_data($this->main_db, $set, $where);
                        if($update){
                            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                        }
                    }
                }else{
                    $msg_detail["username"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            
            $msg_detail["password"]     = strip_tags(form_error('password'));
            $msg_detail["repassword"]   = strip_tags(form_error('repassword'));    
            $msg_detail["token"]        = strip_tags(form_error('token'));     
        }

        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------change_password_admin-----------------------
#===============================================================================
    


}
