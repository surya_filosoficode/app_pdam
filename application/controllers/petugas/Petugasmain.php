<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugasmain extends CI_Controller {
    public $main_db = "m_petugas";
    public $tagihan_db = "tr_tagihan";

    public $GLOBAL_TOKEN;

	public function __construct(){
        parent::__construct(); 
        $this->load->model('petugas/petugas_main', 'um');
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');
        $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        $this->load->library("Globalvar");

        $this->GLOBAL_TOKEN = $this->globalvar->GLOBAL_TOKEN;
        
        // $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------home_petugas--------------------------------
#===============================================================================
    private function val_form_log(){
        $config_val_input = [
                [   'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                       
                ],
                [   'field'=>'password',
                    'label'=>'password',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                       
                ],
                [   'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                ]
            ];
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_petugas_login(){
        // print_r($_POST);
        $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL")];
        $msg_detail = ["username" => "",
                        "password" => "",
                        "token" => ""];

        $cek = [];

        if($this->val_form_log()){
            $username   = $this->input->post('username', true);
            $password   = $this->input->post('password', true);
            $token      = $this->input->post('token', true);

            if($token == $this->GLOBAL_TOKEN){

                $where = [
                        'pass_petugas' => hash("sha256", $password),
                        "is_del_petugas" => "0"
                    ];

                $where_or = [
                        'kd_petugas' => $username,
                        'username_petugas' => $username
                    ];
                
                $cek = $this->um->select_petugas($where, $where_or);
                // $cek["status_log"] = false;
                if($cek){
                    $cek["logged_in"] = 1;
                    if($this->auth_v0->set_session($cek)){
                        $msg_main = ["status" => true, "msg"=>$this->response_message->get_success_msg("LOG_SUC")];
                    }
                }
            }

        }else {
            $msg_detail["username"] = form_error("username");
            $msg_detail["password"] = form_error("password");
            $msg_detail["token"] = form_error("token");

            $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL")];
        }
        // print_r($cek);

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $msg_array["data_session"] = $cek;
        print_r(json_encode($msg_array));
    }
#===============================================================================
#-----------------------------------home_petugas--------------------------------
#===============================================================================
    

#===============================================================================
#-----------------------------------insert_petugas------------------------------
#===============================================================================

    private function val_form_insert_data(){
        $config_val_input = [
                [   'field'=>'id_petugas',
                    'label'=>'id_petugas',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                       
                ],
                [   'field'=>'id_user',
                    'label'=>'id_user',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                       
                ],
                [   'field'=>'id_bumdes',
                    'label'=>'id_bumdes',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                       
                ],
                [   'field'=>'tgl_tr_tagihan',
                    'label'=>'tgl_tr_tagihan',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                ],
                [   'field'=>'periode_tr_tagihan',
                    'label'=>'periode_tr_tagihan',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                ],
                [   'field'=>'meter_tr_tagihan',
                    'label'=>'meter_tr_tagihan',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                ],
                [   'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                ]
            ];
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_data(){
        
        $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL")];
        $msg_detail = ["id_petugas" => "",
                        "id_user" => "",
                        "id_bumdes" => "",
                        "tgl_tr_tagihan" => "",
                        "periode_tr_tagihan" => "",
                        "meter_tr_tagihan" => "",
                        "token" => ""];

        if($this->val_form_insert_data()){


            $id_petugas = $this->input->post("id_petugas" ,true);
            $id_user = $this->input->post("id_user" ,true);
            $id_bumdes = $this->input->post("id_bumdes" ,true);
            $tgl_tr_tagihan = $this->input->post("tgl_tr_tagihan" ,true);
            $periode_tr_tagihan = $this->input->post("periode_tr_tagihan" ,true);
            $meter_tr_tagihan = $this->input->post("meter_tr_tagihan" ,true);
            $token = $this->input->post("token" ,true);

            $tgl_now = date("Y-m-d H:i:s");
            # check token
            if($token == $this->GLOBAL_TOKEN){

                #check bumdes avail or not
                $check_bumdes = $this->mm->get_data_each("m_bumdes", ["id_bumdes"=>$id_bumdes]);
                $tarif_bumdes = $check_bumdes["tarif_bumdes"];
                // $permeter_tr_tagihan = $check_bumdes["permeter"];

                if($check_bumdes){

                    #check petugas avail or not
                    $check_petugas = $this->mm->get_data_each("m_petugas", ["id_petugas"=>$id_petugas]);
                    $petugas_bumdes = $check_petugas["id_bumdes"];
                    if($check_petugas){

                        #check user avail or not
                        $check_user = $this->mm->get_data_each("m_user", ["id_user"=>$id_user]);
                        $user_bumdes = $check_user["id_bumdes"];
                        $disc_user = $check_user["disc_user"];

                        if($check_user){

                            #check user bumdes and petugas bumdes
                            if($id_bumdes == $petugas_bumdes && $id_bumdes == $user_bumdes && $meter_tr_tagihan != 0){
                                // print_r("ok");
                                #check periode tagihan user bumdes
                                $check_periode = $this->mm->get_data_each($this->tagihan_db, ["periode_tr_tagihan"=>$periode_tr_tagihan]);
                                if(!$check_periode){
                                    // print_r("ok");

                                    $nominal_tr_tagihan = (float) $meter_tr_tagihan * (float) $tarif_bumdes;
                                    $nominal_disc = (float) $disc_user / 100 * $nominal_tr_tagihan;
                                    $nominal_fix = $nominal_tr_tagihan - $nominal_disc;

                                    $data = [
                                        "id_tr_tagihan"=>"",
                                        "id_bumdes"=>$id_bumdes,
                                        "id_petugas"=>$id_petugas,
                                        "id_user"=>$id_user,
                                        "tgl_tr_tagihan"=>$tgl_now,
                                        "periode_tr_tagihan"=>$periode_tr_tagihan,
                                        "permeter_tr_tagihan"=>$tarif_bumdes,
                                        "meter_tr_tagihan"=>$meter_tr_tagihan,
                                        "disc_tr_tagihan"=>$disc_user,
                                        "nominal_tr_tagihan"=>$nominal_fix,
                                        "tgl_inp_tr_tagihan"=>$tgl_now,

                                        "create_by_tr_tagihan"=>$id_petugas,
                                        "sts_pemb"=>"0",
                                        "vert_pemb_by"=>"",
                                        "tgl_vert_pemb_by"=>"",
                                        "tgl_up_tr_tagihan"=>"",
                                        "up_by_tr_tagihan"=>""
                                    ];

                                    $insert = $this->mm->insert_data($this->tagihan_db, $data);

                                    if($insert){
                                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                                    }
                                }else{
                                    $msg_main = ["status" => false, "msg"=>"Tagihan pada periode ini $periode_tr_tagihan sudah terinput"];
                                }
                            }else{
                                $msg_main = ["status" => false, "msg"=>"User tidak terdaftar pada data BUMDES saudara"];
                            }

                        }else{
                            $msg_main = ["status" => false, "msg"=>"User tidak terdaftar pada data BUMDES saudara"];
                        }
                    }else{
                        $msg_main = ["status" => false, "msg"=>"Invalid petugas"];
                    }
                }
                    
            }else{
                    $msg_main = ["status" => false, "msg"=>"Invalid access"];
                }


        }else {
            $msg_detail["id_petugas"] = form_error("id_petugas");
            $msg_detail["id_user"] = form_error("id_user");
            $msg_detail["id_bumdes"] = form_error("id_bumdes");
            $msg_detail["tgl_tr_tagihan"] = form_error("tgl_tr_tagihan");
            $msg_detail["periode_tr_tagihan"] = form_error("periode_tr_tagihan");
            $msg_detail["meter_tr_tagihan"] = form_error("meter_tr_tagihan");
            $msg_detail["token"] = form_error("token");

            $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL")];
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#===============================================================================
#-----------------------------------insert_petugas------------------------------
#===============================================================================



#===============================================================================
#-----------------------------------update_petugas------------------------------
#===============================================================================

    public function update_data(){
        
        $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL")];
        $msg_detail = ["id_petugas" => "",
                        "id_user" => "",
                        "id_bumdes" => "",
                        "periode_tr_tagihan" => "",
                        "meter_tr_tagihan" => "",
                        "token" => ""];

        if($this->val_form_insert_data()){
            $id_tr_tagihan = $this->input->post("id_tr_tagihan", true);

            $id_petugas = $this->input->post("id_petugas" ,true);
            $id_user = $this->input->post("id_user" ,true);
            $id_bumdes = $this->input->post("id_bumdes" ,true);
            $periode_tr_tagihan = $this->input->post("periode_tr_tagihan" ,true);
            $meter_tr_tagihan = $this->input->post("meter_tr_tagihan" ,true);
            $token = $this->input->post("token" ,true);

            $tgl_now = date("Y-m-d H:i:s");
            # check token
            if($token == $this->GLOBAL_TOKEN){


                #check bumdes avail or not
                $check_bumdes = $this->mm->get_data_each("m_bumdes", ["id_bumdes"=>$id_bumdes]);
                $tarif_bumdes = $check_bumdes["tarif_bumdes"];
                // $permeter_tr_tagihan = $check_bumdes["permeter"];

                if($check_bumdes){

                    #check petugas avail or not
                    $check_petugas = $this->mm->get_data_each("m_petugas", ["id_petugas"=>$id_petugas]);
                    $petugas_bumdes = $check_petugas["id_bumdes"];
                    if($check_petugas){

                        #check user avail or not
                        $check_user = $this->mm->get_data_each("m_user", ["id_user"=>$id_user]);
                        $user_bumdes = $check_user["id_bumdes"];
                        $disc_user = $check_user["disc_user"];

                        if($check_user){

                            #check user bumdes and petugas bumdes
                            if($id_bumdes == $petugas_bumdes && $id_bumdes == $user_bumdes && $meter_tr_tagihan != 0){
                                // print_r("ok");
                                #check periode tagihan user bumdes
                                $check_periode = $this->mm->get_data_each($this->tagihan_db, ["periode_tr_tagihan"=>$periode_tr_tagihan, "id_tr_tagihan != "=>$id_tr_tagihan]);
                                if(!$check_periode){
                                    // print_r("ok");

                                    $check_tr_tagihan = $this->mm->get_data_each($this->tagihan_db, ["id_tr_tagihan"=>$id_tr_tagihan]);
                                    $sts_pemb = $check_tr_tagihan["sts_pemb"];

                                    if($sts_pemb == "0"){
                                        $nominal_tr_tagihan = (float) $meter_tr_tagihan * (float) $tarif_bumdes;
                                        $nominal_disc = (float) $disc_user / 100 * $nominal_tr_tagihan;
                                        $nominal_fix = $nominal_tr_tagihan - $nominal_disc;

                                        $data = [
                                            "periode_tr_tagihan"=>$periode_tr_tagihan,
                                            "permeter_tr_tagihan"=>$tarif_bumdes,
                                            "meter_tr_tagihan"=>$meter_tr_tagihan,
                                            "disc_tr_tagihan"=>$disc_user,
                                            "nominal_tr_tagihan"=>$nominal_fix,
                                            "tgl_inp_tr_tagihan"=>$tgl_now,
                                            "tgl_up_tr_tagihan"=>$tgl_now,
                                            "up_by_tr_tagihan"=>$id_petugas
                                        ];

                                        $where = ["id_tr_tagihan"=>$id_tr_tagihan];

                                        $update = $this->mm->update_data($this->tagihan_db, $data, $where);

                                        if($update){
                                            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                                        }
                                    }
                                }else{
                                    $msg_main = ["status" => false, "msg"=>"Tagihan pada periode ini $periode_tr_tagihan sudah terinput"];
                                }
                            }else{
                                $msg_main = ["status" => false, "msg"=>"User tidak terdaftar pada data BUMDES saudara"];
                            }

                        }else{
                            $msg_main = ["status" => false, "msg"=>"User tidak terdaftar pada data BUMDES saudara"];
                        }
                    }else{
                        $msg_main = ["status" => false, "msg"=>"Invalid petugas"];
                    }
                }
                    
            }else{
                    $msg_main = ["status" => false, "msg"=>"Invalid access"];
                }


        }else {
            $msg_detail["id_petugas"] = form_error("id_petugas");
            $msg_detail["id_user"] = form_error("id_user");
            $msg_detail["id_bumdes"] = form_error("id_bumdes");
            $msg_detail["tgl_tr_tagihan"] = form_error("tgl_tr_tagihan");
            $msg_detail["periode_tr_tagihan"] = form_error("periode_tr_tagihan");
            $msg_detail["meter_tr_tagihan"] = form_error("meter_tr_tagihan");
            $msg_detail["token"] = form_error("token");

            $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL")];
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#===============================================================================
#-----------------------------------update_petugas------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------check_user----------------------------------
#===============================================================================

    private function val_form_check_user(){
        $config_val_input = [
                [   'field'=>'id_petugas',
                    'label'=>'id_petugas',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                       
                ],[   'field'=>'id_bumdes',
                    'label'=>'id_bumdes',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                       
                ],[   'field'=>'kd_user',
                    'label'=>'kd_user',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                       
                ],[ 'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]
                       
                ]
            ];
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function check_user(){
        // print_r($_POST);
        // die();
        $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL")];
        $msg_detail = ["id_petugas" => "",
                        "id_bumdes" => "",
                        "kd_user" => "",
                        "token" => ""
                    ];

        $data_user = [];
        if($this->val_form_check_user()){
            $id_petugas = $this->input->post("id_petugas" ,true);
            $id_bumdes  = $this->input->post("id_bumdes" ,true);
            $kd_user    = $this->input->post("kd_user" ,true);
            $token      = $this->input->post("token" ,true);

            $tgl_now = date("Y-m-d H:i:s");
            # check token
            if($token == $this->GLOBAL_TOKEN){
                #check user avail or not
                $check_user = $this->mm->get_data_each("m_user", ["REPLACE(kd_user, '.', '') = "=>str_replace('.', '', $kd_user)]);
                if($check_user){

                    // print_r("ok");
                    // die();
                    $id_bumdes_user = $check_user["id_bumdes"];

                    if($id_bumdes_user == $id_bumdes){
                        $data_user = $check_user;
                        $msg_main = ["status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC")];
                    }
                }
                    
            }else{
                    $msg_main = ["status" => false, "msg"=>"Invalid access"];
                }


        }else {
            $msg_detail["id_petugas"]   = form_error("id_petugas");
            $msg_detail["id_bumdes"]    = form_error("id_bumdes");
            $msg_detail["kd_user"]      = form_error("kd_user");
            $msg_detail["token"]        = form_error("token");

            $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL")];
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $msg_array["data_user"] = $data_user;
        print_r(json_encode($msg_array));
    }
#===============================================================================
#-----------------------------------check_user----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------check_user_by_bc----------------------------
#===============================================================================

    public function check_user_by_bc(){
        $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL")];
        $msg_detail = ["id_petugas" => "",
                        "id_bumdes" => "",
                        "kd_user" => "",
                        "token" => ""
                    ];

        $data_user = [];
        if($this->val_form_check_user()){
            $id_petugas = $this->input->post("id_petugas" ,true);
            $id_bumdes  = $this->input->post("id_bumdes" ,true);
            $kd_user    = $this->input->post("kd_user" ,true);
            $token      = $this->input->post("token" ,true);

            $tgl_now = date("Y-m-d H:i:s");
            # check token
            if($token == $this->GLOBAL_TOKEN){
                #check user avail or not
                // print_r("ok");
                // die();

                $check_user = $this->mm->get_data_each("m_user", ["sha2(kd_user, '256') = "=>$kd_user]);
                if($check_user){
                    $id_bumdes_user = $check_user["id_bumdes"];
                    // print_r("ok");
                    // die();
                
                    if($id_bumdes_user == $id_bumdes){
                        $data_user = $check_user;
                        $msg_main = ["status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC")];
                    }
                }
                    
            }else{
                $msg_main = ["status" => false, "msg"=>"Invalid access"];
            }

        }else {
            $msg_detail["id_petugas"]   = form_error("id_petugas");
            $msg_detail["id_bumdes"]    = form_error("id_bumdes");
            $msg_detail["kd_user"]      = form_error("kd_user");
            $msg_detail["token"]        = form_error("token");

            $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL")];
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $msg_array["data_user"] = $data_user;
        print_r(json_encode($msg_array));
    }
#===============================================================================
#-----------------------------------check_user_by_bc----------------------------
#===============================================================================


#===============================================================================
#-----------------------------------list_tagihan_by_user------------------------
#===============================================================================
    public function get_tagihan_by_user(){
        $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL")];
        $msg_detail = ["id_petugas" => "",
                        "id_bumdes" => "",
                        "kd_user" => "",
                        "token" => ""
                    ];

        $data_list = [];
        if($this->val_form_check_user()){
            $id_petugas = $this->input->post("id_petugas" ,true);
            $id_bumdes  = $this->input->post("id_bumdes" ,true);
            $kd_user    = $this->input->post("kd_user" ,true);
            $token      = $this->input->post("token" ,true);

            $tgl_now = date("Y-m-d H:i:s");
            # check token

            if($token == $this->GLOBAL_TOKEN){
                $where = ["REPLACE(mu.kd_user, '.', '') = "=>str_replace('.', '', $kd_user), 
                            "th.id_petugas"=>$id_petugas, 
                            "th.id_bumdes"=>$id_bumdes,
                            "th.sts_pemb"=>"0"];

                $tagihan = $this->ot->get_tr_tagihan_for_api($where);
                if($tagihan){
                    $msg_main = ["status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC")];
                    $data_list = $tagihan;
                }
            }
        }else {
            $msg_detail["id_petugas"]   = form_error("id_petugas");
            $msg_detail["id_bumdes"]    = form_error("id_bumdes");
            $msg_detail["kd_user"]      = form_error("kd_user");
            $msg_detail["token"]        = form_error("token");

            $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL")];
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $msg_array["data_list"] = $data_list;
        print_r(json_encode($msg_array));
    }
#===============================================================================
#-----------------------------------list_tagihan_by_user------------------------
#===============================================================================


#===============================================================================
#-----------------------------------history_tagihan_by_user---------------------
#===============================================================================
    public function get_history_tagihan_by_user(){
        $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL")];
        $msg_detail = ["id_petugas" => "",
                        "id_bumdes" => "",
                        "kd_user" => "",
                        "token" => ""
                    ];

        $data_list = [];
        if($this->val_form_check_user()){
            $id_petugas = $this->input->post("id_petugas" ,true);
            $id_bumdes  = $this->input->post("id_bumdes" ,true);
            $kd_user    = $this->input->post("kd_user" ,true);
            $token      = $this->input->post("token" ,true);

            $tgl_now = date("Y-m-d H:i:s");
            # check token

            if($token == $this->GLOBAL_TOKEN){
                $where = ["REPLACE(mu.kd_user, '.', '') = "=>str_replace('.', '', $kd_user), 
                            "th.id_petugas"=>$id_petugas, 
                            "th.id_bumdes"=>$id_bumdes];

                $tagihan = $this->ot->get_tr_tagihan_for_api($where);
                if($tagihan){
                    $msg_main = ["status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC")];
                    $data_list = $tagihan;
                }
            }
        }else {
            $msg_detail["id_petugas"]   = form_error("id_petugas");
            $msg_detail["id_bumdes"]    = form_error("id_bumdes");
            $msg_detail["kd_user"]      = form_error("kd_user");
            $msg_detail["token"]        = form_error("token");

            $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL")];
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $msg_array["data_list"] = $data_list;
        print_r(json_encode($msg_array));
    }
#===============================================================================
#-----------------------------------history_tagihan_by_user---------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete---------------------
#===============================================================================
    public function delete(){
        $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL")];
        $msg_detail = ["id_petugas" => "",
                        "id_bumdes" => "",
                        "kd_user" => "",
                        "token" => ""
                    ];

        $data_list = [];
        if($this->val_form_check_user()){
            $id_tr_tagihan = $this->input->post("id_tr_tagihan" ,true);

            $id_petugas = $this->input->post("id_petugas" ,true);
            $id_bumdes  = $this->input->post("id_bumdes" ,true);
            $kd_user    = $this->input->post("kd_user" ,true);
            $token      = $this->input->post("token" ,true);

            $tgl_now = date("Y-m-d H:i:s");
            # check token

            if($token == $this->GLOBAL_TOKEN){
                $where = ["th.id_tr_tagihan"=>$id_tr_tagihan, 
                            "th.id_petugas"=>$id_petugas, 
                            "th.id_bumdes"=>$id_bumdes];

                $tagihan = $this->ot->get_tr_tagihan_for_api($where);

                // print_r($tagihan);
                // die();
                if($tagihan){
                    $sts_pemb = $tagihan[0]->sts_pemb;
                    if($sts_pemb == "0"){
                        $delete = $this->mm->delete_data($this->tagihan_db, 
                                ["id_tr_tagihan"=>$id_tr_tagihan, 
                                "id_petugas"=>$id_petugas, 
                                "id_bumdes"=>$id_bumdes]);
                        if($delete){
                            $msg_main = ["status" => true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC")];
                        }
                    }
                    
                    // $data_list = $tagihan;
                }
            }
        }else {
            $msg_detail["id_petugas"]   = form_error("id_petugas");
            $msg_detail["id_bumdes"]    = form_error("id_bumdes");
            $msg_detail["kd_user"]      = form_error("kd_user");
            $msg_detail["token"]        = form_error("token");

            $msg_main = ["status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL")];
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $msg_array["data_list"] = $data_list;
        print_r(json_encode($msg_array));
    }
#===============================================================================
#-----------------------------------delete---------------------
#===============================================================================
}
